#!/bin/env python

'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 15-Dec-2014 18:03:23$
 Version $1.0
 ProjectDir = 
'''

import os,sys,argparse
import bb_pipeline_tools.bb_logging_tool as LT
import bb_pipeline_tools.bb_file_manager as FM

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def bb_pipeline_QSM(subject, jobHold, fileConfiguration):

    logger = LT.initLogging(__file__, subject)
    logDir  = logger.logDir
    baseDir = logDir[0:logDir.rfind('/logs/')]

    jobQSM_1 = LT.runCommand(logger, 
            '${FSLDIR}/bin/fsl_sub -q short.qc@@short.hge  -N "bb_QSM_'   + 
            subject + '" -j ' + str(jobHold)  + ' -l ' + logDir + 
            ' $BB_BIN_DIR/bb_QSM_pipeline/bb_QSM_main -subjdir ' + subject +
            ' -GDC 1 -ne 2')
    
    return jobQSM_1

def main(): 
  
    parser = MyParser(description='BioBank QSM Tool')
    parser.add_argument("subjectFolder", help='Subject Folder')
    parser.add_argument("-n", "--normcheck", action="store_false", default=True,
                        help='Do NOT check Normalisation in structural image (default if flag not used: Check normalisation)', 
                        dest="norm_check")
    parser.add_argument("-P", "--namingPatterns", action="store", nargs="?",
                        default=os.environ['BB_BIN_DIR'] + "/bb_data/naming_pattern_UKBB.json",
                        help='Do NOT check Normalisation in structural image (default if flag not used: Check normalisation)', 
                        dest="naming_patterns")

    argsa = parser.parse_args()

    subject = argsa.subjectFolder
    subject = subject.strip()
    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]

    logger = LT.initLogging(__file__, subject)
    logger.info('Running file manager') 

    # Check normalisation argument
    norm_check = argsa.norm_check
    
    # Naming pattern argument
    naming_patterns = argsa.naming_patterns
    naming_patterns = naming_patterns.strip()
    if not os.path.exists(naming_patterns):
        logger.error("Subject cannot be run. Incorrect naming pattern file specified: " + naming_patterns)
        LT.finishLogging(logger)
        exit


    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]
    
    fileConfig = FM.bb_file_manager(subject, False, norm_check, naming_patterns)

    jobSTEP1 = bb_pipeline_QSM(subject, '-1', fileConfig)
    
    print(jobSTEP1)
             
if __name__ == "__main__":
    main()
