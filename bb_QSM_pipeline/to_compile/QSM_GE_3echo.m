 function QSM_GE_3echo(subjdir,mask_name,TE1,TE2,TE3)
    %% load data

    TEs(1)=str2double(TE1);
    TEs(2)=str2double(TE2);
    TEs(3)=str2double(TE3);
   
    REAL_name=dir([subjdir '/SWI/*REAL*TE*.nii.gz']);
    IMAG_name=dir([subjdir '/SWI/*IMAGINARY*TE*.nii.gz']);
    
    for j=1:3
   
        real(:,:,:,j)=double(niftiread([subjdir '/SWI/' REAL_name(j).name]));
        imag(:,:,:,j)=double(niftiread([subjdir '/SWI/' IMAG_name(j).name]));

    end

    temp=real+1i*imag;

    clear j real imag

    kspace=fftn(temp);
    compc=ifftn(fftshift(kspace,3));
    phasec=angle(compc);
    mag=abs(compc);

    clear compc kspace temp

    info=niftiinfo([subjdir '/SWI/' mask_name '.nii.gz']);
    info.Datatype='double';

    niftiwrite(double(phasec(:,:,:,1)), [subjdir '/SWI/QSM/PHASE_TE1.nii'], ...
               info, 'Compressed', true);
    niftiwrite(double(phasec(:,:,:,2)), [subjdir '/SWI/QSM/PHASE_TE2.nii'], ...
               info, 'Compressed', true);
    niftiwrite(double(phasec(:,:,:,3)), [subjdir '/SWI/QSM/PHASE_TE3.nii'], ...
               info, 'Compressed', true);
           
    niftiwrite(double(mag(:,:,:,1)), [subjdir '/SWI/QSM/MAG_TE1.nii'], ...
               info, 'Compressed', true);
    niftiwrite(double(mag(:,:,:,2)), [subjdir '/SWI/QSM/MAG_TE2.nii'], ...
               info, 'Compressed', true);
    niftiwrite(double(mag(:,:,:,3)), [subjdir '/SWI/QSM/MAG_TE3.nii'], ...
               info, 'Compressed', true);
    
    setenv('FSLOUTPUTTYPE', 'NIFTI_GZ');

    FSLDIR = getenv('FSLDIR');
    
    system([FSLDIR, '/bin/fslmaths ', subjdir, ...
            '/SWI/' mask_name '.nii.gz -thr 0.6 -bin ', subjdir, ...
            '/SWI/QSM/QSM_mask.nii.gz']);
    system([FSLDIR, '/bin/prelude -a ', subjdir, '/SWI/QSM/MAG_TE1.nii.gz -p ', ...
            subjdir, '/SWI/QSM/PHASE_TE1.nii.gz -u ', subjdir, ...
            '/SWI/QSM/UWPHASE_TE1.nii.gz -m ', subjdir, '/SWI/QSM/QSM_mask.nii.gz']);
    system([FSLDIR, '/bin/prelude -a ', subjdir, '/SWI/QSM/MAG_TE2.nii.gz -p ', ...
            subjdir, '/SWI/QSM/PHASE_TE2.nii.gz -u ', subjdir, ...
            '/SWI/QSM/UWPHASE_TE2.nii.gz -m ', subjdir, '/SWI/QSM/QSM_mask.nii.gz']);
    system([FSLDIR, '/bin/prelude -a ', subjdir, '/SWI/QSM/MAG_TE3.nii.gz -p ', ...
            subjdir, '/SWI/QSM/PHASE_TE3.nii.gz -u ', subjdir, ...
            '/SWI/QSM/UWPHASE_TE3.nii.gz -m ', subjdir, '/SWI/QSM/QSM_mask.nii.gz']);
    
    delete([subjdir '/SWI/QSM/MAG_TE1.nii.gz']);
    delete([subjdir '/SWI/QSM/MAG_TE2.nii.gz']);
    delete([subjdir '/SWI/QSM/MAG_TE3.nii.gz']);
        
    uwphase(:,:,:,1)=double(niftiread([subjdir '/SWI/QSM/UWPHASE_TE1.nii.gz']));
    uwphase(:,:,:,2)=double(niftiread([subjdir '/SWI/QSM/UWPHASE_TE2.nii.gz']));
    uwphase(:,:,:,3)=double(niftiread([subjdir '/SWI/QSM/UWPHASE_TE3.nii.gz']));

    delete([subjdir '/SWI/QSM/UWPHASE_TE1.nii.gz']);
    delete([subjdir '/SWI/QSM/UWPHASE_TE2.nii.gz']);
    delete([subjdir '/SWI/QSM/UWPHASE_TE3.nii.gz']);
    
    mask = double(niftiread([subjdir '/SWI/QSM/QSM_mask.nii.gz']));

    %% QSM
    dcm_name=dir([subjdir '/DICOM/SWI*.dcm']);
    dcm_info = dicominfo([subjdir '/DICOM/' dcm_name.name]);
    ori = dcm_info.ImageOrientationPatient;
    Xz = ori(3);
    Yz = ori(6);
    Zxyz = cross(ori(1:3), ori(4:6));
    Zz = Zxyz(3);
    H = [-Xz, -Yz, Zz];

    voxsz = [dcm_info.PixelSpacing' dcm_info.SliceThickness];
    B0 = dcm_info.MagneticFieldStrength;

    clear ori Xz Yz Zxyz Zz dcm_name;

    map = phasevariance_nonlin(mask, phasec(:,:,:,1), 2);
    map2 = phasevariance_nonlin(mask, phasec(:,:,:,2), 2);
    map3 = phasevariance_nonlin(mask, phasec(:,:,:,3), 2);

    dim = size(phasec(:,:,:,1));

    mask(map < 0.7) = 0;
    mask(map2 < 0.6) = 0;
    mask(map3 < 0.45) = 0;

    for ii = 1:dim(3)
        mask(:, :, ii) =  bwareaopen(mask(:, :, ii), 300);
        mask(:, :, ii) =~ bwareaopen(~mask(:, :, ii), 50);
    end
    mask = imfill(mask,26,'holes');
% remove 2pi jump between echoes
    unph_diff = uwphase(:,:,:,2) - uwphase(:,:,:,1);

    for echo = 2:3
        meandiff = uwphase(:,:,:,echo)-uwphase(:,:,:,1)-double(echo-1)*unph_diff;
        meandiff = meandiff(mask==1);
        meandiff = mean(meandiff(:));
        njump = round(meandiff/(2*pi));
    
        uwphase(:,:,:,echo) = uwphase(:,:,:,echo) - njump*2*pi;
        uwphase(:,:,:,echo) = uwphase(:,:,:,echo).*mask;
    end

    clear unph_diff meandiff

    lfs = dBfit(uwphase, TEs);
    
%background field removal
    [dB_vsf, mask_vsf]=V_SHARP(lfs, mask, 'voxelsize', voxsz, 'smvsize', 12);

    dB_vsf = double(dB_vsf);
    mask_vsf = double(mask_vsf);
   
    clear mask;

    mask_vsf(map < 0.7) = 0;
    mask_vsf(map2 < 0.7) = 0;
    mask_vsf(map3 < 0.55) = 0;

    for ii = 1:dim(3)
        mask_vsf(:, :, ii) = bwareaopen(mask_vsf(:, :, ii), 200);
        mask_vsf(:, :, ii) =~ bwareaopen(~mask_vsf(:, :, ii), 20);
    end
    mask_vsf = imfill(mask_vsf,26,'holes');

    clear map phs_comb map2 map3

    qsm_iLSQR_vsf_inv = QSM_iLSQR(-dB_vsf, mask_vsf, 'TE', 1, 'B0', B0, ...
                'H', H, 'padsize', [64 64 64], ...
                'voxelsize', voxsz);
    info.Datatype='single';
    niftiwrite(single(qsm_iLSQR_vsf_inv * 1000), [subjdir '/SWI/QSM/QSM.nii'], ...
               info, 'Compressed', true);


end
