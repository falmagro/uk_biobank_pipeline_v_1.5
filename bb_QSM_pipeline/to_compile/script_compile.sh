#!/bin/bash

set -x

file=$1

date;

rm -rf   $PWD/comp_$file/;
mkdir -p $PWD/comp_$file/for_testing;

mcc -w disable -o $file -W main:$file -T link:exe \
    -d $PWD/comp_$file/for_testing \
    -R '-singleCompThread' -R '-nodisplay'  \
    -v $PWD/$file.m \
    -a $PWD/STISuite_V3.0/Core_Functions_P \
    -a $PWD/STISuite_V3.0/Support_Functions \
    -a $PWD/Matlab_Scripts 

chmod 755 $PWD/comp_$file/for_testing/*
cp $PWD/comp_$file/for_testing/$file ../Matlab_Compiled/

rm -rf $PWD/comp_$file/

date;


