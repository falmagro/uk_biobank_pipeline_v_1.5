function combined = weightedCombination(image, sensitivity)

    image = double(image);
    sensitivity = double(sensitivity);

    dimension = size(image);
    combined = zeros(dimension(1:(end - 1)), 'double');

    for iChannel = 1:dimension(4)
        combined = combined + image(:, :, :, iChannel) .* sensitivity(:, :, :, iChannel);
    end
    combined = combined ./ sqrt(abs(combined));

end
