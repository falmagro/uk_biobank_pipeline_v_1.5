function dB = dBfit(phase, TEs)

[sx,sy,sz,ne] = size(phase);

phase = permute(phase,[4 1 2 3]);

phase = reshape(phase,ne,[]);

TE_rep = repmat(TEs(:),[1 sx*sy*sz]);

TE_rep=nets_demean(TE_rep,1);
phase=nets_demean(phase,1);

dB = sum(phase.*TE_rep,1)./(sum(TE_rep.*TE_rep)+eps);
dB = reshape(dB,[sx sy sz]);

end
