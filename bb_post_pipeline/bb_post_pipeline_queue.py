#!/bin/env python

'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 11-Dec-2020
 Version $1.0
 ProjectDir = 
 '''

import sys
import argparse
import bb_pipeline_tools.bb_logging_tool as LT
from bb_pipeline_tools.bb_file_manager import bb_file_manager

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def submit(logger, queues, nameJob, logDir, jobHold, command, subject):
    return LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + queues + ' -N "' + 
                        nameJob + subject + '" -l ' + logDir + ' -j ' + jobHold +
                        ' ' + command + ' ' + subject)

def bb_post_pipeline(subject, jobHold, fileConfiguration, queue):

    logger = LT.initLogging(__file__, subject)
    logDir  = logger.logDir

    if not queue == "normal": 
        short_queues = ','.join(['short.qc@@' + x for x in queue])
        #long_queues  = ','.join(['long.qc@@'  + x for x in queue])
    else:
        short_queues = 'short.qc@@short.hge'
        #long_queues  = 'long.qc@@long.hge'

    with open(logDir + 'jobs_POST.txt', 'w') as writer:
        writer.write('$BB_BIN_DIR/bb_post_pipeline/bb_T1_brain_to_MNI_linear '   + subject + '\n')
        writer.write('$BB_BIN_DIR/bb_post_pipeline/bb_T1_intnorm '               + subject + '\n')
        writer.write('$BB_BIN_DIR/bb_post_pipeline/bb_T1_FS_Smooth_Thckn_Area '  + subject + '\n')
        writer.write('$BB_BIN_DIR/bb_post_pipeline/bb_rfMRI_dualreg2 '           + subject + '\n')
        writer.write('$BB_BIN_DIR/bb_post_pipeline/bb_tfMRI_to_MNI '             + subject + '\n')
        writer.write('$BB_BIN_DIR/bb_post_pipeline/bb_dMRI_tract_sum '           + subject + '\n')
        writer.write('$BB_BIN_DIR/bb_post_pipeline/bb_SWI_T2star_to_MNI '        + subject + '\n')
        writer.write('$BB_BIN_DIR/bb_post_pipeline/bb_T2_FLAIR_lesions_to_MNI '  + subject + '\n')
        writer.write('$BB_BIN_DIR/bb_post_pipeline/bb_T2_FLAIR_PeriDeepWMH '     + subject + '\n')

    job1 = submit(logger, short_queues, 'bb_POST_pipeline', logDir, jobHold, ' -t ' + logDir + 'jobs_POST.txt', '')
    job2 = submit(logger, short_queues, 'bb_POST_QC_',      logDir, job1, '$BB_BIN_DIR/bb_QC_scripts//bb_QC_T1 ',  subject)
    job3 = submit(logger, short_queues, 'bb_POST_clean_',   logDir, job2, '$BB_BIN_DIR/bb_general_tools/bb_clean_subject_keep_SWI ', subject)
   
    return '' + job1 + ',' + job2 + ',' + job3


def main(): 
  
    parser = MyParser(description='BioBank Post Pipeline')
    parser.add_argument("subjectFolder", help='Subject Folder')
    parser.add_argument("queue", help='queue_modifier', nargs="?", default="normal")
    parser.add_argument("jobID", help='jobID to hold for', nargs="?", default="-1")

    argsa = parser.parse_args()

    subject = argsa.subjectFolder
    subject = subject.strip()

    # If subject name ends with / --> remove it
    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]

    # Default queue scheme is "normal"
    if argsa.queue == []:
        queue = "normal"
    elif argsa.queue == 'normal':
        queue = "normal"
    else:
        queue = argsa.queue.split(",")

    # If no specified job ID to wait for wait for -1 (do not wait)
    if argsa.jobID == []:
        jobID = "-1"
    else:
        jobID = argsa.jobID

    logger = LT.initLogging(__file__, subject)

    logger.info('Running file manager') 
    fileConfig = bb_file_manager(subject, False, False)

    logger.info("File configuration after running file manager: " + str(fileConfig))

    if not (('T1' in fileConfig ) and (fileConfig['T1'] != '')):
        logger.error("Subject cannot be run. There is no T1 or it has not the correct dimensions")
        LT.finishLogging(logger)
        exit

    jobID=bb_post_pipeline(subject, jobID, fileConfig, queue)

    LT.finishLogging(logger)
    print(jobID)

if __name__ == "__main__":
    main()
