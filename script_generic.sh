#!/usr/bin/env bash

source /fbp/bb_pipeline_v_2.5/init_vars

. $BB_BIN_DIR/bb_pipeline_tools/bb_set_header 

set +x

# Update the pipeline with the latest version
pushd $BB_BIN_DIR;
git pull;
popd;

export SUBJID="$1"
export VISIT="$2"

if [ "$VISIT" == "" ] ;	then
    export VISIT="2"
fi

cd /ukbdata/;

$BB_BIN_DIR//bb_pipeline_tools/bb_pipeline_queue.py $SUBJID -X $3

. $BB_BIN_DIR/bb_pipeline_tools/bb_set_footer
