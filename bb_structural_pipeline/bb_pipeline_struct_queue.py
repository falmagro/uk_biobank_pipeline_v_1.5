#!/bin/env python

'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 15-Dec-2014 18:03:23
 Version $1.0
 ProjectDir = 
 '''

import os
import numpy as np
import bb_pipeline_tools.bb_logging_tool as LT

def bb_pipeline_struct(subject, runTopup, fileConfiguration, queue, coeff):

    logger  = LT.initLogging(__file__, subject)
    logDir  = logger.logDir
    jobSTRUCTINIT="-1"
    jobSWI="-1"

    if not queue == "normal": 
        short_queues = ','.join(['short.qc@@' + x for x in queue])
        long_queues  = ','.join(['long.qc@@'  + x for x in queue])
    else:
        short_queues = 'short.qc@@short.hge'
        long_queues  = 'long.qc@@long.hge'

    if (not 'T1' in fileConfiguration) or (fileConfiguration['T1'] == ''):
        logger.error('There is no T1. Subject ' + subject + ' cannot be processed.')
        return -1
    
    else:
        # Create the B0 AP - PA file to estimate the fieldmaps
        b0_threshold=int(np.loadtxt(os.environ['BB_BIN_DIR'] + "/bb_data/b0_threshold.txt"))   

        jobsB0=[]

        if runTopup:
            for encDir in ['AP', 'PA']:
                bvals=np.loadtxt(subject +"/dMRI/raw/" + encDir + ".bval")
                if not type(bvals) == list:
                    numVols = 1
                else:
                    numVols=int(sum(bvals<=b0_threshold))

                if not os.path.isdir(subject + '/fieldmap/'):
                    os.makedirs(subject + '/fieldmap/')
                jobGETB01 = LT.runCommand(logger,   '${FSLDIR}/bin/fsl_sub -q ' + short_queues + ' -N "bb_get_b0s_1_'       + subject + '" -l ' + logDir + ' $BB_BIN_DIR/bb_structural_pipeline/bb_get_b0s.py -i ' + subject + '/dMRI/raw/' + encDir + '.nii.gz -o ' + subject + '/fieldmap/total_B0_' + encDir + '.nii.gz -n ' + str(numVols) + ' -l ' + str(b0_threshold) )
                jobsB0.append(LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues  + ' -N "bb_choose_bestB0_1_' + subject + '" -l ' + logDir + ' -j ' + jobGETB01  + ' $BB_BIN_DIR/bb_structural_pipeline/bb_choose_bestB0 ' + subject + '/fieldmap/total_B0_' + encDir + '.nii.gz ' + subject + '/fieldmap/B0_' + encDir + '.nii.gz '))

            jobMERGE = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + short_queues + ' -N "bb_fslmerge_' + subject + '" -j ' + ",".join(jobsB0) +' -l ' + logDir + ' ${FSLDIR}/bin/fslmerge -t ' + subject + '/fieldmap/B0_AP_PA ' + subject + '/fieldmap/B0_AP ' + subject + '/fieldmap/B0_PA' )

        # Registrations - T1 to MNI - T2 to T1 - T2 to MNI (Combining the 2 previous ones)
        jobSTRUCTINIT = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues + ' -N "bb_structinit_' + subject + '" -l ' + logDir + ' $BB_BIN_DIR/bb_structural_pipeline/bb_struct_init ' + subject + ' ' + coeff)        

        #TODO: Do a better check here. This one looks arbitrary
        if 'SWI_TOTAL_MAG_TE2' in fileConfiguration:
            jobSWI = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues + ' -N "bb_swi_reg_' + subject + '" -l ' + logDir + ' -j ' + jobSTRUCTINIT + ' $BB_BIN_DIR/bb_structural_pipeline/bb_swi_reg ' + subject  + ' ' + coeff)

        # Topup
        if runTopup:
            jobPREPAREFIELDMAP = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + short_queues + ' -N "bb_prepare_struct_fieldmap_' + subject + '" -l ' + logDir + ' -j ' + jobMERGE + ' $BB_BIN_DIR/bb_structural_pipeline/bb_prepare_struct_fieldmap ' + subject )
            jobTOPUP = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues + ' -N "bb_topup_' + subject + '" -l ' + logDir + ' -j ' + jobPREPAREFIELDMAP + ' ${FSLDIR}/bin/topup --imain=' + subject + '/fieldmap/B0_AP_PA --datain=' + subject + '/fieldmap/acqparams.txt --config=b02b0.cnf --out=' + subject + '/fieldmap/fieldmap_out --fout=' + subject + '/fieldmap/fieldmap_fout --jacout=' + subject + '/fieldmap/fieldmap_jacout -v ')
            
        else:
            logger.error("There is not enough/correct DWI data. Topup cannot be run. fMRI and DWI cannot be run")

        if not runTopup:
            return ",".join([jobSTRUCTINIT, jobSWI])
        else:
            jobPOSTTOPUP = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues + ' -N "bb_post_topup_' + subject + '" -l ' + logDir + ' -j ' + jobTOPUP + ',' + jobSTRUCTINIT + ',' + jobSWI + ' $BB_BIN_DIR/bb_structural_pipeline/bb_post_topup ' + subject + ' ' + coeff)
            return jobPOSTTOPUP

