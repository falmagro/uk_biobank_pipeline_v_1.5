#!/bin/env python
'''
 FMRIB, Oxford University
 $13-Jan-2021 10:41:10$
 Version $1.0
 ProjectDir = 
 '''

import sys
import argparse
import numpy as np

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def bb_make_int(fileName):
    try:
        with open(fileName, 'r') as f:
            values = np.loadtxt(f)
            dims = values.ndim
    except Exception as e:
        print("Error in bb_make_int: There was a problem opening file: " + fileName + " | " + str(e))
        sys.exit()

    try:
        if dims == 1:
            values_int = [int(np.round(x)) for x in values]
        elif dims == 2:
            values_int = values.astype(int)           
        else:
            print("Error in bb_make_int: There was Something wrong in the file: " + str(values))
            sys.exit()

    except Exception as e:
        print("Error in bb_make_int: There was a problem converting values: " + str(values) + " | " + str(e))
        sys.exit()

    try:
        with open(fileName, 'w') as f:
            if dims == 1:
                f.writelines(" ".join([str(x) for x in values_int]) + '\n')
            else:
                for j in range(values_int.shape[0]):
                    f.writelines(" ".join([str(x) for x in values_int[j,:]]) + '\n')

    except Exception as e:
        print("Error in bb_make_int: There was a problem creating output: " + fileName + " | " + str(e))
        sys.exit()

def main(): 

    parser = MyParser(description='BioBank Integer conversion')
    parser.add_argument("file", help='File that we want in integer form')

    argsa = parser.parse_args()
    fileName = argsa.file

    bb_make_int(fileName)


if __name__ == "__main__":
    main()
