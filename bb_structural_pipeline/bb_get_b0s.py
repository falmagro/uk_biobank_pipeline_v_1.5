#!/bin/env python
'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 $01-May-2014 11:44:20$
 Version $1.0
 ProjectDir = 
 '''

import sys
import os.path
import argparse
from bb_make_int import bb_make_int
from subprocess import check_output

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main():    
    parser = MyParser(description='UK Biobank tool to get a B0 of a set of B0 images')
    parser.add_argument('-i', dest="inputFile", type=str, nargs=1, help='Input File')
    parser.add_argument('-o', dest="outputFile", type=str, nargs=1, help='Output File')
    parser.add_argument('-n', dest='desiredNumber',type=int, nargs=1,help='Desired number of B0s from file. If none specified, all will be selected')
    parser.add_argument('-l', dest='B0limit',type=int, default=[100], nargs=1,help='Limit B0 value. (Default 100)')
    parser.add_argument('-a', dest='bvalFilename',type=str, default='', nargs=1,help='bval file. (Default: Same basename as the input file)')
    
    argsa = parser.parse_args()
    
    if (argsa.inputFile==None):
        parser.print_help()
        exit()
    
    if (argsa.outputFile==None):
        parser.print_help()
        exit()
    
    baseDir = os.path.dirname(argsa.inputFile[0])
    outDir=os.path.dirname(argsa.outputFile[0])
    baseN = os.path.basename(argsa.inputFile[0]).split('.')[0]
    outN = os.path.basename(argsa.outputFile[0]).split('.')[0]
    
    if argsa.bvalFilename == '':
        bvalFilename = baseDir + "/" + baseN+".bval"
    else: 
        bvalFilename = argsa.bvalFilename[0]
    
    bb_make_int(bvalFilename)

    f = open(bvalFilename)
    
    line = f.readlines()
 
    line = line[0].split()
    
    B0_intesity_limit = int(argsa.B0limit[0])
        
    indices = [i for i, x in enumerate(line) if int(x) < B0_intesity_limit]
    
    if (argsa.desiredNumber==None):
      desiredNumber = len(indices)
    else:
      desiredNumber = argsa.desiredNumber[0]

    if desiredNumber > len(indices):
        print(("There are only %i B0. It is not possible to have %i" % (len(indices), desiredNumber)))
        exit()
        
    if desiredNumber <= 0:
        print("The number of B0 must be positive")
        exit()

    f = open (outDir + '/' + outN +'_indices.txt' ,'w')
    f.write(" ".join([str(x) for x in indices]))
    f.close()

    indices = indices[0:desiredNumber]

    check_output('$FSLDIR/bin/fslselectvols -i ' + argsa.inputFile[0] + ' -o ' + argsa.outputFile[0] + ' --vols=' + ','.join(str(i) for i in indices), shell=True)

if __name__ == "__main__":
    main()

