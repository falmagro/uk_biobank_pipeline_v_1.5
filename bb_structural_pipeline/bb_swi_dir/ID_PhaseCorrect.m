
function swiProcessing(dirName, workingDir, niftilib)
    addpath(niftilib);

    %% data dims  FA: Better getting it from the data itself
    magImgFileName = [workingDir, '/', dirName,'/SWI/SWI_TOTAL_MAG_TE2_orig.nii.gz'];
    magImg = load_untouch_nii(magImgFileName);
    xDim = size(magImg,1);
    yDim = size(magImg,2);
    sliceDim = size(magImg,3);

    nbChan = 32;
    TE2 = 19.7;
    TE1 = 9.42;

    magLongImgDir =  [workingDir, '/', dirName, '/SWI/MAG_TE2/'];
    magShortImgDir = [workingDir, '/', dirName, '/SWI/MAG_TE1/'];
    phaLongImgDir =  [workingDir, '/', dirName, '/SWI/PHA_TE2/'];
    phaShortImgDir = [workingDir, '/', dirName, '/SWI/PHA_TE1/'];

    fileName = 'SWI_3MM_UPDATED_V1.1_COIL';

    phaseDiffResu = complex(zeros(yDim,xDim,sliceDim),zeros(yDim,xDim,sliceDim));

    for indChan = 1:nbChan
        shortMagFile = [magShortImgDir,fileName,num2str(indChan,'%02i'),'_ECHO1_17.nii.gz'];
        shortPhaFile = [phaShortImgDir,fileName,num2str(indChan,'%02i'),'_ECHO1_20.nii.gz'];
        longMagFile =  [magLongImgDir, fileName,num2str(indChan,'%02i'),'_ECHO2_17.nii.gz'];
        longPhaFile =  [phaLongImgDir, fileName,num2str(indChan,'%02i'),'_ECHO2_20.nii.gz'];
        resu1 = load_untouch_nii(shortMagFile);
        resu2 = load_untouch_nii(shortPhaFile);
        resu3 = load_untouch_nii(longMagFile);
        resu4 = load_untouch_nii(longPhaFile);
        
        phaseDiff = double(resu3.img) .* exp(1i*((double(resu4.img)-2047)/2048*pi)) .* exp(-1i*((double(resu2.img)-2047)/2048*pi));
        phaseDiffResu = phaseDiffResu + phaseDiff;
    end

    phaseDiffResu = phaseDiffResu.*exp(1i*TE2/(TE2-TE1)); % scale phase image with corresponding TE
    PhaseDiffResu = angle(phaseDiffResu);

    outName = [workingDir, '/', dirName, '/SWI/unfiltered_phase.nii.gz'];   
    %save_avw(PhaseDiffResu,fname,'f',voxsize);
    save_nii(make_nii(PhaseDiffResu), outName);
    call_fsl(['fslcpgeom ' magImgFileName ' ' outName]);

