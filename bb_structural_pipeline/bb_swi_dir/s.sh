export SWIVERSION="2.00"
export FSL_SWIDIR="$BB_BIN_DIR/bb_structural_pipeline/bb_swi_dir";
export FSL_SWI_MATLAB_ROOT=/mcr/v901;

export FSL_SWI_MATLAB=${FSL_SWI_MATLAB_ROOT}/bin/matlab
export FSL_SWI_MCC=${FSL_SWI_MATLAB_ROOT}/bin/mcc
export FSL_SWI_MCRROOT=$MCRROOT
export FSL_SWI_MCRV=`cat ${FSL_SWIDIR}/MCR.version`

export FSL_SWI_MCR=/mcr/v901/

export FSL_SWI_MLCDIR=${FSL_SWIDIR}/compiled/Linux/x86_64
export FSL_SWI_MLOPTS="-nodisplay -nodesktop -nosplash"

export FSL_SWI_MLEVAL="-r"
export FSL_SWI_MLFILE="\<"

export FSL_SWI_OCTAVE=/usr/bin/octave
export FSL_SWI_OCOPTS="--traditional -q --no-window-system"
export FSL_SWI_OCEVAL="--eval"
export FSL_SWI_OCFILE=""

export FSL_SWI_MATLAB_MODE=0
export FSL_SWI_CIFTIRW="$BB_BIN_DIR/bb_ext_tools/workbench/CIFTIMatlabReaderWriter";
export FSL_SWI_WBC="$BB_BIN_DIR/bb_ext_tools/workbench//bin_linux64/wb_command";

export FSL_SWI_CIFTIRW FSL_SWI_WBC

export FSL_SWI_FSLMATLAB=${FSLDIR}/etc/matlab

export MCRROOT="/mcr/v901"
export LD_LIBRARY_PATH=.:${MCRROOT}/runtime/glnxa64 ;
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRROOT}/bin/glnxa64 ;
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRROOT}/sys/os/glnxa64;
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRROOT}/sys/opengl/lib/glnxa64;
