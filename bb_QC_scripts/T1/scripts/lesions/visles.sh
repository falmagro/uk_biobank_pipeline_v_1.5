#!/bin/sh

if [ $# -lt 1 ] ; then
  echo "Usage: `basename $0` <lesionmask> <manualmask> <string for initial FSLView>"
  echo "Example string for initial FSLView is: T1W_H0_R0_restore PD_H0_R0_restore PseudoFlair -b 0,1"
  echo "Use NOFSLVIEW to turn off generation of images and invokation of fslview"
  exit 1
fi

lesmask=`$FSLDIR/bin/remove_ext $1`
manualmask=`$FSLDIR/bin/remove_ext $2`
shift 2;
dofslview=yes
allims="$@"
if [ "X$allims" = "XNOFSLVIEW" ] ; then
    dofslview=no
fi

# generate FP lesion mask (all clusters in lesionmask that do not overlap with anything in manualmask)
$FSLDIR/bin/cluster -t 0.5 -i $lesmask --connectivity=6 --oindex=visoutmask1 --no_table
maxc=`$FSLDIR/bin/fslstats visoutmask1 -P 100`
$FSLDIR/bin/fslmaths $manualmask -thr 0.5 -bin -mul visoutmask1 visoutmask2
maxv=`echo $maxc + 0.5 | bc -l`
maxc1=`echo $maxc + 1 | bc`
histvals=`$FSLDIR/bin/fslstats visoutmask2 -H $maxc1 -0.5 $maxv | sed 's/\.0*//g'`
if [ $dofslview = yes ] ; then
    $FSLDIR/bin/fslmaths $lesmask -mul 0 visoutFP
fi
n=0;
fptot=0
for val in $histvals ; do
    if [ $n -gt 0 ] ; then
	if [ $val -eq 0 ] ; then
	    fptot=`echo $fptot + 1 | bc`
	    if [ $dofslview = yes ] ; then 
		echo "FP for cluster number $n (out of $maxc)"
	    fi
	else
	    # for now accumulate TP clusters (as there are less, so this is quicker)
	    if [ $dofslview = yes ] ; then
		$FSLDIR/bin/fslmaths visoutmask1 -thr $n -uthr $n -bin -add visoutFP visoutFP
	    fi
	fi
    fi
    n=`echo $n + 1 | bc`;
done
nles=$n
if [ $dofslview = yes ] ; then
    # now subtract the accumulated (TP) clusters from the total set
    $FSLDIR/bin/fslmaths $lesmask -thr 0.5 -bin -sub visoutFP visoutFP
fi

imrm visoutmask1 visoutmask2

# generate FN lesion mask (all clusters in manualmask that do not have any overlap with anything in lesionmask)
$FSLDIR/bin/cluster -t 0.5 -i $manualmask --connectivity=6 --oindex=visoutmask1 --no_table
maxc=`$FSLDIR/bin/fslstats visoutmask1 -P 100`
$FSLDIR/bin/fslmaths $lesmask -thr 0.5 -bin -mul visoutmask1 visoutmask2
maxv=`echo $maxc + 0.5 | bc -l`
maxc1=`echo $maxc + 1 | bc`
histvals=`$FSLDIR/bin/fslstats visoutmask2 -H $maxc1 -0.5 $maxv | sed 's/\.0*//g'`
if [ $dofslview = yes ] ; then
    $FSLDIR/bin/fslmaths $manualmask -mul 0 visoutFN
fi
n=0;
fntot=0
for val in $histvals ; do
    if [ $n -gt 0 ] ; then
	if [ $val -eq 0 ] ; then
	    fntot=`echo $fntot + 1 | bc`
	    if [ $dofslview = yes ] ; then
		echo "FN for cluster number $n (out of $maxc)"
		$FSLDIR/bin/fslmaths visoutmask1 -thr $n -uthr $n -bin -add visoutFN visoutFN
	    fi
	fi
    fi
    n=`echo $n + 1 | bc`;
done
nman=$n

#imrm visoutmask1 visoutmask2

echo "Stats are: FP = $fptot out of $nles positives ; FN = $fntot out of $nman manually segmented lesions"

if [ $dofslview = yes ] ; then
    # view the results in FSLView (allow aliases and different paths for FSLView here)
    echo "Legend: lesionmask = Yellow ; manual mask = Green ; FP = Red ; FN = Blue"
    echo "Interpretation: anything in Yellow is part of a true positive lesion, but with borders bigger than the manual mask"
    fslview $allims $lesmask -l Yellow $manualmask -l Green visoutFP -l Red visoutFN -l Blue
fi
