/*  preproc.cc

    Mark Jenkinson, FMRIB Image Analysis Group

    Copyright (C) 2003 University of Oxford  */

/*  CCOPYRIGHT  */

// Pre-processing for lesion segmentation

#define _GNU_SOURCE 1
#define POSIX_SOURCE 1

#include "newimage/newimageall.h"
#include "miscmaths/miscmaths.h"
#include "utils/options.h"

using namespace MISCMATHS;
using namespace NEWIMAGE;
using namespace Utilities;

// The two strings below specify the title and example usage that is
//  printed out as the help or usage message

string title="preproc (Version 1.0)\nCopyright(c) 2012, University of Oxford (Mark Jenkinson)";
string examples="preproc [options] -i <imagename> -o <outname>";

// Each (global) object below specificies as option and can be accessed
//  anywhere in this file (since they are global).  The order of the
//  arguments needed is: name(s) of option, default value, help message,
//       whether it is compulsory, whether it requires arguments
// Note that they must also be included in the main() function or they
//  will not be active.

Option<bool> verbose(string("-v,--verbose"), false, 
		     string("switch on diagnostic messages"), 
		     false, no_argument);
Option<bool> help(string("-h,--help"), false,
		  string("display this message"),
		  false, no_argument);
Option<string> inname(string("-i,--in"), string(""),
		  string("input filename"),
		  true, requires_argument);
Option<string> outname(string("-o,--out"), string(""),
		  string("output filename"),
		  true, requires_argument);
Option<string> mode(string("-m,--mode"), string("3x3"),
		  string("specify mode as one of: 3x3, 9x9, 3x3x3, 9x9x9"),
		  false, requires_argument);
int nonoptarg;

////////////////////////////////////////////////////////////////////////////

// Local functions

volume<float> moving_average(const volume<float>& im, int nker, int dir) {
  // use the efficient circular storage (one in, one out) method
  double *buff;
  buff = new double[nker];
  volume<float> retim(im);
  int n2=(nker-1)/2;

  if (dir==1) {
    for (int z=im.minz(); z<=im.maxz(); z++) {
      for (int y=im.miny(); y<=im.maxy(); y++) {
	double movsum=0.0;
	int m=1, den=0;  // m is index for circular buffer
	for (int n=1; n<=nker; n++) { buff[n-1]=0.0; }
	for (int x=1; x<=im.xsize()+n2; x++) {
	  if (x>nker) { movsum-=buff[m-1]; den--; }
	  if (x<=im.xsize()) {
	    buff[m-1]=im(x-1,y,z);
	    movsum+=buff[m-1];  
	    den++;
	  }
	  if ((x-n2)>=1) retim(x-n2-1,y,z)=movsum/den;
	  m++;  if (m>nker) m=1;
	}
      }
    }
  }

  if (dir==2) {
    for (int z=im.minz(); z<=im.maxz(); z++) {
      for (int x=im.minx(); x<=im.maxx(); x++) {
	double movsum=0.0;
	int m=1, den=0;  // m is index for circular buffer
	for (int n=1; n<=nker; n++) { buff[n-1]=0.0; }
	for (int y=1; y<=im.ysize()+n2; y++) {
	  if (y>nker) { movsum-=buff[m-1]; den--; }
	  if (y<=im.ysize()) {
	    buff[m-1]=im(x,y-1,z);
	    movsum+=buff[m-1];  
	    den++;
	  }
	  if ((x-n2)>=1) retim(x,y-n2-1,z)=movsum/den;
	  m++;  if (m>nker) m=1;
	}
      }
    }
  }

  if (dir==3) {
    for (int y=im.miny(); y<=im.maxy(); y++) {
      for (int x=im.minx(); x<=im.maxx(); x++) {
	double movsum=0.0;
	int m=1, den=0;  // m is index for circular buffer
	for (int n=1; n<=nker; n++) { buff[n-1]=0.0; }
	for (int z=1; z<=im.zsize()+n2; z++) {
	  if (z>nker) { movsum-=buff[m-1]; den--; }
	  if (z<=im.zsize()) {
	    buff[m-1]=im(x,y,z-1);
	    movsum+=buff[m-1];  
	    den++;
	  }
	  if ((x-n2)>=1) retim(x,y,z-n2-1)=movsum/den;
	  m++;  if (m>nker) m=1;
	}
      }
    }
  }
  
  delete [] buff;
  return retim;
}

// for example ... print difference of COGs between 2 images ...
int do_work(int argc, char* argv[]) 
{
  volume<float> v1, retv;
  read_volume(v1,inname.value());
  int dims=2;
  int size=3;
  if ((mode.value()=="3x3x3") || (mode.value()=="9x9x9")) { dims=3; }
  if ((mode.value()=="9x9") || (mode.value()=="9x9x9")) { size=9; }
  
  retv = moving_average(v1,size,1);
  retv = moving_average(retv,size,2);
  if (dims==3) {
    retv = moving_average(retv,size,3);
  }
  save_volume(retv,outname.value());
  return 0;
}

////////////////////////////////////////////////////////////////////////////

int main(int argc,char *argv[])
{

  Tracer tr("main");
  OptionParser options(title, examples);

  try {
    // must include all wanted options here (the order determines how
    //  the help message is printed)
    options.add(inname);
    options.add(outname);
    options.add(mode);
    options.add(verbose);
    options.add(help);
    
    nonoptarg = options.parse_command_line(argc, argv);

    // line below stops the program if the help was requested or 
    //  a compulsory option was not set
    if ( (help.value()) || (!options.check_compulsory_arguments(true)) )
      {
	options.usage();
	exit(EXIT_FAILURE);
      }
    
  }  catch(X_OptionError& e) {
    options.usage();
    cerr << endl << e.what() << endl;
    exit(EXIT_FAILURE);
  } catch(std::exception &e) {
    cerr << e.what() << endl;
  } 

  // Call the local functions

  return do_work(argc,argv);
}

