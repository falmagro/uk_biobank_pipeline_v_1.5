%% MUST PASS IN:  training, num_features

%% disp('Loading data');
%% load Pz1Cn21_alltissue
%% training=[newvecs(:,166) newvecs(:,1:162) newvecs(:,163:165)/100];

% the numbers below are the feature numbers of the central intensity values!
criticalfeatures=[6 33 60 141];

disp('Verifying data');
[N,M1]=size(training);
% training is the training set matrix (first column are labels)
% N is number of training cases
M=M1-1; % M is the number of variables in the classifier (less
        % output flag)

% mm is the number to use in one particular decision tree
%% What is a good way of choosing this?!?
%% Loop over mm?   %% for mm=round(M/50):round(M/3),
%% mm=M/5;  
mm=num_features;

NT=500;  % number of trees
%% Also need a way of choosing/optimising this number

disp('Initial setup');
samples=1:N;
examplestruct=struct(treefit(training(:,1:3),training(samples,1),'method', 'classification','splitmin',2));
examplestruct=struct(treeprune(examplestruct,'level',0));
forest(1:NT)=examplestruct;
forestf=zeros(NT,mm);
correct=zeros(N,1);
total=zeros(N,1);

disp('Starting training');
for nn=1:NT,
  % make a sample training set with bagging (size N)
  samples=max(ceil(rand(N,1)*N),1);
  % work out where the used and unused samples were
  unusedsamps=ones(N,1);
  unusedsamps(samples)=0;
  unusedsamps=logical(unusedsamps);
  % select a random set of mm features to use (no repetition)
  fundfeature=0;
  while (fundfeature==0),
    features=randperm(M);
    features=features(1:mm)+1;
    sumf=0;
    %%%% for cf=1:length(criticalfeatures),
    %%%%   sumf=sumf+sum(features==criticalfeatures(cf));
    %%%% end
    fundfeature=1;
    if (sumf>0), fundfeature=1; end
    if (M<2*length(criticalfeatures)),  fundfeature=1; end
  end
  % grow a tree from randomly selected mm features - one per set
  forest(nn)=struct(treefit(training(samples,features),training(samples,1),'method','classification','splitmin',2));
  forest(nn)=struct(treeprune(forest(nn),'level',0));
  forestf(nn,1:mm)=features;
  %  evaluate the performance on the unused samples (need to
  %  accumulate results and take the mode later)
  % Note: res has -1 at end as treeval gives [1,2] not [0,1]
  res=treeval(forest(nn),training(unusedsamps,features))-1;
  correct(unusedsamps)=correct(unusedsamps)+(res==training(unusedsamps,1));
  total(unusedsamps)=total(unusedsamps)+1;
  if (mod(nn,10)==0), disp([num2str(nn/NT*100),' %']); end
end

disp('Finished training');
%final (internally assessed) result for this random forest
mean(correct./max(total,1))

%% save workspace

