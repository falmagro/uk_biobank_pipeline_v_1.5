/*  rf_lda.cc

    Mark Jenkinson, FMRIB Image Analysis Group

    Copyright (C) 2011 University of Oxford  */

/*  CCOPYRIGHT  */

// Random Forest with LDA trees

#define _GNU_SOURCE 1
#define POSIX_SOURCE 1

#include "newimage/newimageall.h"
#include "miscmaths/miscmaths.h"
#include "utils/options.h"

using namespace MISCMATHS;
using namespace NEWIMAGE;
using namespace Utilities;

// The two strings below specify the title and example usage that is
//  printed out as the help or usage message

string title="rf_lda (Version 1.0)\nCopyright(c) 2011, University of Oxford (Mark Jenkinson)";
string examples="rf_lda [options] -i <input matrix>";

// Each (global) object below specificies as option and can be accessed
//  anywhere in this file (since they are global).  The order of the
//  arguments needed is: name(s) of option, default value, help message,
//       whether it is compulsory, whether it requires arguments
// Note that they must also be included in the main() function or they
//  will not be active.

Option<bool> verbose(string("-v,--verbose"), false, 
		     string("switch on diagnostic messages"), 
		     false, no_argument);
Option<bool> help(string("-h,--help"), false,
		  string("display this message"),
		  false, no_argument);
Option<bool> debug(string("--debug"), false,
		  string("turn on debugging mode"),
		  false, no_argument);
Option<bool> nobagging(string("--nobagging"), false,
		  string("do not use bagging when creating trees"),
		  false, no_argument);
Option<bool> uselda(string("--uselda"), false,
		  string("use LDA as an option for tree splitting"),
		  false, no_argument);
Option<bool> ignore_critical_features(string("--ignore_critical_features"), false,
		  string("ignore any critical features specified in the inputlist file"),
		  false, no_argument);
Option<int> labelcol(string("--labelcol"), 0,
		  string("number of the column in the large data that contains labels (if it exists)"),
		  false, requires_argument);
Option<int> nfeat(string("-n"), 0,
		  string("number of features in each tree [use 0 for sqrt(num features)]"),
		  false, requires_argument);
Option<int> n_trees(string("-t,--ntrees"), 5,
		  string("number of trees in forest"),
		  false, requires_argument);
Option<int> seed(string("-s"), 1,
		  string("set random seed"),
		  false, requires_argument);
Option<int> maxtreesize(string("--maxtreesize"), 1000,
		  string("set maximum tree size (number of rows; default 1000)"),
		  false, requires_argument);
Option<float> tolerance(string("--minfraction"), 0.01,
		  string("a minimum fraction for splitting (below which a leaf is formed; default 0.01)"),
		  false, requires_argument);
Option<string> featureimportance(string("--featureimportance"), string(""),
		  string("calculate and save the feature importance statistics (columns are TP, FP, TN, FN)"),
		  false, requires_argument);
Option<string> labelvol(string("--labelvol"), string(""),
		  string("load separate label volume for testing"),
		  false, requires_argument);
Option<string> criticalfeaturemat(string("--criticalfeaturemat"), string(""),
		  string("input filename of critical feature values [in form: n_crit n_start n_end]"),
		  false, requires_argument);
Option<string> load_rf(string("--loadrf"), string(""),
		  string("random forest file to use for testing"),
		  false, requires_argument);
Option<string> outname(string("-o"), string(""),
		  string("output basename (for training is random forest ; for testing is classification)"),
		  false, requires_argument);
Option<string> inputlist(string("--inputlist"), string(""),
		  string("filename of files containing a list of images to use for testing or training data (plus 3 extra columns : 1st is type of data [0=centre intensity; 1=3x3 neighbourhood; n=3x3 set of voxels separated by n voxels]; 2nd and 3rd are normalisation values"),
		  false, requires_argument);
Option<string> testdataname(string("--testdatamat"), string(""),
		  string("input test data filename in matrix format"),
		  false, requires_argument);
Option<string> trainingdataname(string("--trainingdatamat"), string(""),
		  string("input training data filename in matrix format"),
		  false, requires_argument);
int nonoptarg;


////////////////////////////////////////////////////////////////////////////

// Some global variables (naughty, I know... but could be made into class members and so therefore OK?)

Matrix trainingdata, trainingdata_full, critical_features;
vector<vector<int> > sorted_indices;
vector<int> ranks;
vector<int> global_dummy_vec;
int nattr;
int global_nfeat=0;
int initcol=1;
float global_attr_vec[1000];  // assume we never have more features than this!
volume<float> mask;
Matrix featimp, featnum;  // feature importance and feature numbers per tree (optional)

////////////////////////////////////////////////////////////////////////////

// General functions

int urand(int min, int max) 
{
  int MAXRAND=2147483647;  // = 2^31 - 1
  int mrand=Max(Min(ceil((double(random())/MAXRAND)*(max-min+1)),(max-min+1)),1)+min-1;  
  return mrand;
}

// Discrimination-related functions

float apply_disc(const Matrix& data, const RowVector& disc_vec, float disc_thr, int nrow)  
{
  // this function is intentionally isolated from the global trainingdata (for better reuse)
  // disc_vec should always be nattr long (and have no label in it)
  float val=0.0;
  for (int n=1; n<=nattr; n++) { val+=data(nrow,n+initcol)*disc_vec(n); }
  val-=disc_thr;
  return val;
}

int calc_split(const vector<int>& rows, const RowVector& disc_vec, float disc_thr, 
	       int& nup, int& nlow, int& correctup, int& correctlow, 
	       vector<int>& rowsupper, vector<int>& rowslower, bool makerows=true) 
{
  // this assumes that dataupper and datalower are "big enough" and that the calling
  //  function can deal with truncation if needed
  nup=0; nlow=0;
  correctlow=0; correctup=0;
  if (makerows) { rowsupper.clear(); rowslower.clear(); }
  for (unsigned int n=0; n<rows.size(); n++) {
    if (apply_disc(trainingdata,disc_vec,disc_thr,rows[n])>0) {  // NB: this is a function now to avoid inefficient SubMatrix calls
      nup++;
      if (trainingdata(rows[n],1)>1.5) { correctup++; }
      if (makerows) rowsupper.push_back(rows[n]);  // dataupper.SubMatrix(nup,nup,1,1+nattr) = data.SubMatrix(n,n,1,1+nattr);
    } else {
      nlow++;
      if (trainingdata(rows[n],1)<1.5) { correctlow++; }
      if (makerows) rowslower.push_back(rows[n]);  // datalower.SubMatrix(nlow,nlow,1,1+nattr) = data.SubMatrix(n,n,1,1+nattr);
    }
  }
  return 0;
}

int calc_split_fractions(const vector<int>& rows, const RowVector& disc_vec, float disc_thr,
			 float& frac_up, float& frac_low, int& nup, int& nlow)
{
  int correctup=0, correctlow=0;
  int retval = calc_split(rows,disc_vec,disc_thr,nup,nlow,correctup,correctlow,global_dummy_vec,global_dummy_vec,false);
  if (nlow>0) frac_low=((float) correctlow)/nlow; else frac_low=0;
  if (nup>0)  frac_up=((float) correctup)/nup; else frac_up=0;
  return retval;
}

float calc_disc_cost(float fraction)
{
  // this is the cost associated with a single node (not a split)
  // fraction should be a value between 0.0 and 1.0
  // Use the Gini cost (as it is very close to the Information Gain but more efficient)
  // Gini = 1 - \sum_i f_i^2
  // Information Gain = - \sum_i f_i log2(f_i)
  //   where f_i is the fraction of elements in partion "i"  (for our case i=1,2)
  //      (and so f1+f2=0 or f2=1-f1)
  // infogain = -f1*log2(f1) -(1-f1)*log2(1-f1);
  float gini = 1.0 - fraction*fraction - (1-fraction)*(1-fraction);
  if (debug.value()) { if ((fraction<0.0) || (fraction>1.0)) { cout << "Fraction outside of [0,1]" << endl; } }
  return gini;
}

float calc_disc_cost(float frac_low, float frac_up, int nlow, int nup)
{
  // this is the cost associated with a split
  // totlow and totup are the number of elements in the two split nodes (indep of classification)
  int ntot=nlow+nup;
  float totgini;
  totgini = calc_disc_cost(frac_low)*((float) nlow)/((float) ntot) + calc_disc_cost(frac_up)*((float) nup)/((float) ntot);
  return totgini;
}

float calc_disc_cost(const vector<int>& rows, const RowVector& disc_vec, float disc_thr)
{
  float gini=0, f1=0, flow=0, fup=0;
  int nlow=0, nup=0;
  f1=calc_split_fractions(rows,disc_vec,disc_thr,fup,flow,nup,nlow);
  // infogain = -f1*log2(f1) -(1-f1)*log2(1-f1);
  gini = calc_disc_cost(flow,fup,nlow,nup);
  return gini;
}

int calc_lda(const vector<int>& rows, RowVector& lda_vec, float& lda_thr) 
{
  if (debug.value()) { cout << "Inside calc_lda" << endl; }
  if (debug.value()) { cout << "Training data size: " << trainingdata.Nrows() << " by " << trainingdata.Ncols() << endl; }
  if (debug.value()) { cout << "  nattr = " << nattr << endl; }
  int n1=0, n2=0;
  ColumnVector w;
  RowVector mean1(nattr), mean2(nattr), vec(nattr);
  Matrix sig1(nattr,nattr), sig2(nattr,nattr);
  mean1=0.0;  mean2=0.0;  vec=0.0;  sig1=0.0;  sig2=0.0;
  for (unsigned int n=0; n<rows.size(); n++) {
    vec = trainingdata.SubMatrix(rows[n],rows[n],2,1+nattr);
    if (trainingdata(rows[n],1)<1.5) {
      mean1 += vec;
      sig1 += vec.t() * vec;
      n1++;
    } else {
      mean2 += vec;
      sig2 += vec.t() * vec;
      n2++;
    }
  }
  if (debug.value()) { cout << "calc_lda: n1,n2 = " << n1 << "," << n2 << endl; }
  if (n1>0) { 
    mean1 /= n1;
    sig1 = (sig1 - n1*mean1.t()*mean1)/(n1-1);
  }
  if (n2>0) { 
    mean2 /= n2;
    sig2 = (sig2 - n2*mean2.t()*mean2)/(n2-1);
  }
  if ((n1>0) && (n2>0) && ((sig1+sig2).Determinant()>1e-10)) {
    w = (sig1+sig2).i()*(mean2 - mean1).t();
  } else {
    w = mean1.t()*0.0;
  }
  Matrix mthr = (mean2 + mean1)*w/2.0;
  lda_thr = mthr.AsScalar();
  lda_vec = w.t();
  return 0;
}

int set_subset_ranks(const vector<int>& rows, int attr) 
{
  // initialises the ranks vector to the appropriate subset
  // attr \in [1,nattr]
  for (unsigned int rk=0; rk<ranks.size(); rk++) { ranks[rk]=0; }  // need to zero this everytime initially
  // if (debug.value()) { 
  //   cout << "Inside set_subset_ranks: sorted_indices[" << attr-1 << "] = "; 
  //   for (int mj=0; mj<rows.size(); mj++) { cout << sorted_indices[attr-1][mj] << " "; } 
  //   cout << endl << endl; 
  //   cout << "                       : rows[m] = "; 
  //   for (int mj=0; mj<rows.size(); mj++) { cout << rows[mj] << " "; } 
  //   cout << endl << endl; 
  // }
  // in general rows is a list which may contain repeated entries (due to bagging)
  // sorted_indices should still be a list of unique numbers
  for (unsigned int m=0; m<rows.size(); m++) { 
    int overall_rank = (sorted_indices[attr-1])[rows[m]-1];   // attr \in [1,N] but vector index from 0 ; output is rank \in [1,N]
    ranks[overall_rank-1] = rows[m];  // store row number of fully bagged row number
  }
  // verify that this way of getting a sorted sample actually works (TEST CODE)
  int rkrow=0, oldrow=-1;
  while (rkrow<(int)ranks.size()) {
    while (ranks[rkrow]==0) { 
      rkrow++; 
      //if (rkrow>=ranks.size()) { cerr << "  ERR: rkrow overflow in set_subset_ranks test" << endl; }
    }
    if (rkrow<(int)ranks.size()) {
      if (oldrow<0) oldrow=rkrow;
      if (trainingdata(ranks[oldrow],attr+1)>trainingdata(ranks[rkrow],attr+1)) { 
	cerr << "ERROR in set_subset_ranks:: sorted values in wrong order: " << trainingdata(ranks[oldrow],attr+1) << " " << trainingdata(ranks[rkrow],attr+1) << endl;
      }
      oldrow=rkrow;
      rkrow++;
    }
  }
  return 0;
}

int start_row_rank() { return -1; } // used with next_row_rank to iterate from beginning

int next_row_rank(int rkrow) {
  int newrkrow=rkrow;
  do {
    newrkrow++; 
    if (newrkrow>=(int)ranks.size()) { return ranks.size(); }
  } while (ranks[newrkrow]==0);
  return newrkrow;
}

int prev_row_rank(int rkrow) {
  int newrkrow=rkrow;
  do {
    newrkrow--; 
    if (newrkrow<0) { return -1; }
  } while (ranks[newrkrow]==0);
  return newrkrow;
}

bool valid_row_rank(int rkrow) {
  return ((rkrow>=0) && (rkrow<(int)ranks.size()));
}


int calc_best_disc(const vector<int>& rows, RowVector& disc_vec, float& disc_thr) 
{
  RowVector lda_vec, attr_vec(nattr);
  float lda_thr, cost=0, newcost=0;
  if (uselda.value()) {
    // start with LDA
    calc_lda(rows,lda_vec,lda_thr);
    if (debug.value()) { cerr << "   ... end LDA" << endl; }
    disc_vec=lda_vec;
    disc_thr=lda_thr;
    cost = calc_disc_cost(rows,lda_vec,lda_thr);
    if (debug.value()) {
      cout << "LDA cost is " << cost << endl;
      cerr << "   threshold = " << disc_thr << endl;
      cerr << "   vector = " << disc_vec << endl;
    }
  } else {
    disc_vec=attr_vec*0.0;
    disc_thr=0.0;
    cost = calc_disc_cost(0.5);  // worst case fraction (so this default will be ignored)
  }
  // now try the standard decision tree splits and see if anything is better
  int bestattr=0, bestrkrow=0;
  float attr_thr=0.0;
  for (int n=1; n<=nattr; n++) {
    // for each attribute try each data value as a threshold 
    //   (ideally pick threshold between this value and the next highest, but this will do for now)
    attr_vec=0.0;
    attr_vec(n)=1.0;
    int totlow=0, totup=0, truelow=0, falselow=0, crow=0, nsplitlow=0;
    set_subset_ranks(rows,n);
    //if (debug.value()) { cout << "Rows.size = " << rows.size() << endl; }
    //if (debug.value()) { cout << "Result of set_subset_ranks(" << n << ") = "; for (int mj=0; mj<rows.size(); mj++) { cout << ranks[mj] << " "; } cout << endl; }
    //if (debug.value()) { cout << endl << endl << "Result of set_subset_ranks(" << n << ") = "; for (int mj=0; mj<rows.size(); mj++) { if (ranks[mj]>0) { cout << trainingdata(ranks[mj],n+1) << " "; } } cout << endl; }
    for (unsigned int m=0; m<rows.size(); m++) { 
      // now count up total true low and up
      if (trainingdata(rows[m],1)<1.5) totlow++; else totup++;
    }
    unsigned int rkrow=start_row_rank(), rkrow2; // this is the correct initialisation for next_row_rank
    truelow=0; falselow=0;
    for (unsigned int m=0; m<rows.size(); m++) {
      // find the next non-zero entry in ranks, which will give the appropriate bagged data row
      rkrow=next_row_rank(rkrow);
      // also get the next value along
      rkrow2=next_row_rank(rkrow);
      
      // test code
      if (valid_row_rank(rkrow) && valid_row_rank(rkrow2) &&
	  (trainingdata(ranks[rkrow2],n+1)<trainingdata(ranks[rkrow],n+1))) { 
	cerr << "ERROR in calc_best_disc:: sorted values in wrong order" << endl; 
      }

      if (!valid_row_rank(rkrow)) { cerr << "ERROR in calc_best_disc:: premature invalid rkrow" << endl; }
      
      //if (debug.value()) { cout << "m , crow, ranks[rkrow], rkrow / max = " << m << " , " << crow << " , " << ranks[rkrow] << " , " << rkrow << " / " << ranks.size() << endl; } 
      crow = ranks[rkrow];
      if (valid_row_rank(rkrow2)) {
	attr_thr = 0.5*(trainingdata(ranks[rkrow2],n+1) + trainingdata(ranks[rkrow],n+1)); 
      } else {
	attr_thr = trainingdata(ranks[rkrow],n+1); 
      }
      // have the number of "low" samples less than *or equal* to the current threshold 
      //   (this threshold gets adjusted to be between this value and the next higher one)
      nsplitlow++;
      if (trainingdata(crow,1)<1.5) truelow++; else falselow++;  // check label (current sample treated as "low" now)
      
      // Do nothing else in this loop unless the next value is *greater than* and not equal to, to avoid having 
      //   thresholds "in the middle" of a set of equal values
      if (valid_row_rank(rkrow) && valid_row_rank(rkrow2) &&
	  (trainingdata(ranks[rkrow2],n+1)>trainingdata(ranks[rkrow],n+1))) {
	
	int nsplitup=totlow+totup-nsplitlow;
	float frac_low=0, frac_up=0;
	if (nsplitlow>0) frac_low= ((float) truelow)/nsplitlow;  // total correct in "low" node
	if (nsplitup>0)  frac_up = ((float) (totup-falselow))/nsplitup;  // total correct in "up" node
	newcost = calc_disc_cost(frac_low,frac_up,nsplitlow,nsplitup);
	// TEST CODE
	// float frac2 = calc_split_fraction(rows,attr_vec,trainingdata(crow,n+1));
	// if (fabs(Min(fraction,1-fraction) - Min(frac2,1-frac2))>0.001) { 
	//   cerr << "ERROR in calc_best_disc:: fraction inconsistent with calc_split_fraction (" << fraction << " vs " << frac2 << ")" << endl; 
	//   if ((fabs(Min(fraction,1-fraction) - Min(frac2,1-frac2))>0.25) && (attr_thr>0)) { 
	//     cerr << "   totlow, totup  = " << totlow << " , " << totup << endl;
	//     cerr << "   truelow, falselow  = " << truelow << " , " << falselow << endl;
	//     cerr << "   Attribute number  = " << n << " , Threshold = " << attr_thr << endl;
	//     cerr << "   Attribute vector  = " << attr_vec << endl;
	//     cerr << "   Values are: ";
	//     for (int mj=0; mj<rows.size(); mj++) { cerr << trainingdata(rows[mj],n+initcol) << " "; }
	//     cerr << endl << "  Labels are: " << endl;
	//     for (int mj=0; mj<rows.size(); mj++) { cerr << trainingdata(rows[mj],1) << " "; }
	//     cerr << endl << "  Rows are: " << endl;
	//     for (int mj=0; mj<rows.size(); mj++) { cerr << rows[mj] << " "; }
	//     cerr << endl << "  Row ranks are:" << endl;
	//     int rkrow0=start_row_rank();
	//     for (int mj=0; mj<rows.size(); mj++) { rkrow0=next_row_rank(rkrow0); 
	//       if (valid_row_rank(rkrow0)) cerr << ranks[rkrow0] << " "; 
	//     }
	//     cerr << endl << "  Ranked values are:" << endl;
	//     rkrow0=start_row_rank();
	//     for (int mj=0; mj<rows.size(); mj++) { rkrow0=next_row_rank(rkrow0); 
	//       if (valid_row_rank(rkrow0)) cerr << trainingdata(ranks[rkrow0],n+1) << " "; 
	//     }
	//     cerr << endl << endl;
	//     exit(EXIT_FAILURE);
	//   }
	// }
	// END TEST CODE
	if (newcost<cost) {
	  if (debug.value()) {
	    cerr << " Best cost so far = " << newcost << endl;
	    cerr << "   threshold = " << attr_thr << endl;
	    cerr << "   frac_low, frac_up  = " << frac_low << " , " << frac_up << endl;
	    cerr << "   nsplitlow, nsplitup  = " << nsplitlow << " , " << nsplitup << endl;
	    cerr << "   truelow, falselow  = " << truelow << " , " << falselow << endl;
	  }
	  cost=newcost;
	  bestattr=n;
	  bestrkrow=rkrow;
	  disc_vec=attr_vec;
	  disc_thr=attr_thr;
	}
      } // if have greater than and not equal
    }  // for loop for m (valid rows)
  } // for loop of n (attr)
  if (debug.value()) { cout << "Best cost = " << cost << endl; }
  if (debug.value()) { cout << "  Final disc_thr = " << disc_thr << endl; }

  return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////

// Tree-related functions

// Represent trees by a Matrix
// each row is a node and links to two more nodes by specifying row numbers
// each row also needs to store the LDA vector and scalar threshold (or none/flag for a leaf)
// leaf nodes (rows) need to contain the class associated with them (have classes be 1 or 2, leaving 0 as a flag)
// LDA TREE FORMAT:
//    Lower Child Row | Upper Child Row | scalar threshold | LDA vector (w)
//    0 | 0 | class of this leaf node | 0 vector
// First row is:
//    -1 | number of valid rows | number of features in training data | 0 vector


// calculate, for each node (row) of the tree, what feature is used
// store -1 if multiple features (lda) or the column number (starts at 4 = "feature number" + 3)
// this is here just for efficiency (as this checking of features would be done multiple times otherwise)
ColumnVector precalc_nonzero_features(const Matrix& tree)
{
  ColumnVector fidx(tree.Nrows());
  fidx=0.0;
  for (int n=1; n<=tree.Nrows(); n++) {
     if (tree(n,1)>0.5) {
        for (int m=4; m<=tree.Ncols(); m++) { 
          if (tree(n,m)!=0.0) { 
             if (fidx(n)>0.5) { 
               fidx(n)=-1.0;   // flag that this has multiple non-zero entries
             } else { 
               fidx(n)=m; 
             }
          }
        }
        if (fidx(n)<0.0) fidx(n)=0.0;
     }
  }
  return fidx;
 }

// returns either 1 or 2 (as stored in the tree's nodes) or -1 upon error
int evaluate_tree(const Matrix& tree, const ColumnVector& fidx, const Matrix& attr_mat, int row)
{
  int crow=2;
  int nattr=attr_mat.Ncols()-initcol;  
  int niter=1, maxiter=500;
  // Quite a few hand optimisations here to speed things up
  // the basic algorithm just needs:
    // for (int n=4; n<=3+nattr; n++) { val += tree(crow,n)*attr_mat(row,n-3); }

  while ((niter++<maxiter) && (tree(crow,1)>0.5)) {
    float val=0.0, w;
    if (fidx(crow)<0.5) {  // this vector has more than two non-zero entries
      for (int n=4; n<=3+nattr; n++) { w=tree(crow,n); if (w!=0.0) { val += w * attr_mat(row,n-3+initcol); } }
    } else {
      if (fidx(crow)>nattr+3.5) { cerr << "ERROR: overflow of fidx (" << fidx(crow) << ") in evaluate_tree" << endl; cerr << " ... occurred at row " << crow << endl; }
      val = tree(crow,MISCMATHS::round(fidx(crow))) * attr_mat(row,MISCMATHS::round(fidx(crow))-3+initcol);
    }
    val -= tree(crow,3);
    if (val>0.0) {
      crow = MISCMATHS::round(tree(crow,2));
    } else {
      crow = MISCMATHS::round(tree(crow,1));
    }
  }
  int retval=-1;
  if ((tree(crow,1)<0.5) && (niter<maxiter)) {
    retval = MISCMATHS::round(tree(crow,3));
  }
  return retval;
}

int evaluate_tree(const Matrix& tree, const Matrix& attr_mat, int row)
{
  ColumnVector fidx;
  fidx = precalc_nonzero_features(tree);
  return evaluate_tree(tree,fidx,attr_mat,row);
}

int addrow(Matrix& tree, RowVector& newrow, int row) 
{
  // used to grow a matrix without invoking the very slow &= operator
  if (row<=tree.Nrows()) {
    tree.SubMatrix(row,row,1,tree.Ncols()) = newrow;
  } else {
    Matrix tmp(Max(4,tree.Nrows()*2),tree.Ncols());  // max so that empty matrices are OK
    tmp = 0.0;
    if (tree.Nrows()>0) { tmp.SubMatrix(1,tree.Nrows(),1,tree.Ncols()) = tree; }
    tree = tmp;
  }
  return 0;
}


int build_lda_tree(Matrix& tree, const vector<int>& rows, int crow, float tol=0.01) 
{
  // FORMAT of data matrix is: first column = labels (1 or 2), then remaining columns are feature/attribute values
  // pass in rows, which is the subset of rows that from the whole data matrix to be used here as current data
  // crow is the row of the *tree* to process, not the data
  // rows are a subset of the row numbers to the *full bagged* data (global variable called trainingdata)
  int MAXSIZE=maxtreesize.value();
  float disc_thr=0.0;
  RowVector newrow(nattr+3), disc_vec(nattr);
  newrow=0.0;
  // on the very first time around initialise and call itself
  if (crow<=1) {
    Matrix tmp(2,nattr+3);
    tmp = 0.0;
    tree = tmp;
    tree(1,1) = -1.0;
    tree(1,2) = 2.0;
    tree(1,3) = nattr;
    return build_lda_tree(tree,rows,2,tol);
  }
  //if (debug.value()) { cout << "build_lda_tree: crow=" << crow << " ; data is " << rows.size() << " x " << data.Ncols() << endl;}
  //if (debug.value()) { cout << "rows from data are: " << endl << rows << endl;}
  // on subsequent calls, fill in the newrow
  float ratio=0.0;
  for (unsigned int m=0; m<rows.size(); m++) { ratio+=trainingdata(rows[m],1); } // data.Column(1).Sum()
  ratio = ratio/rows.size()-1.0;  // based on labels being 1 and 2  (want ratio \in [0,1])
  if (debug.value()) { cout << "ratio = " << ratio << endl; }
  // check if there is a sufficient mix to try and separate
  if ( (ratio<tol) || (ratio>1.0-tol) ) {
    // make this into a leaf node
    tree(crow,1)=0.0;
    tree(crow,2)=0.0;
    tree(crow,3) = (ratio>0.5) ? 2 : 1;
    tree.SubMatrix(crow,crow,4,3+nattr)*=0.0;
    return 0;
  }
  // otherwise call LDA and fill in the current row
  calc_best_disc(rows,disc_vec,disc_thr);
  // split the current data matrix into parts
  int nup=0, nlow=0, cup=0, clow=0;
  vector<int> rowsupper, rowslower;
  rowsupper.clear();  rowslower.clear();
  calc_split(rows, disc_vec, disc_thr, nup, nlow, cup, clow, rowsupper, rowslower);
  // if one part of the split is empty (discriminator fails to separate) then current node is a leaf
  bool emptyhalf=false;
  int modal_label=0;
  if (rowslower.size()==0) { emptyhalf=true; }
  if (rowsupper.size()==0) { emptyhalf=true; rowsupper=rowslower; }
  // also check to see if the tree is just getting too big (and if so make a leaf too)
  if ((emptyhalf) || (MISCMATHS::round(tree(1,2))>MAXSIZE)) {
    if (emptyhalf) {
      if (debug.value()) { cout << "build_lda_tree: making a leaf (empty lda half)" << endl; }
      // get most common label from upper subset  (may have been set to the lower subset above)
      float val=0.0;
      for (unsigned int n=0; n<rowsupper.size(); n++) { 
	if (trainingdata(rowsupper[n],1)>1.5) { val+=1.0; } else { val-=1.0; }
      }
      modal_label = (val>0.0) ? 2 : 1;
    } else {
      if (debug.value()) { cout << "build_lda_tree: making a leaf (tree too big)" << endl; }
      modal_label = (ratio>0.5) ? 2 : 1;
    }
    // turn the current row (node) into a leaf
    tree(crow,1)=0.0;
    tree(crow,2)=0.0;
    tree(crow,3) = modal_label;
    tree.SubMatrix(crow,crow,4,3+nattr)*=0.0;
    return 0;
  }
  // otherwise fill in the rest of current node
  if (debug.value()) { cout << "build_lda_tree: splitting" << endl; }
  tree(crow,1)=MISCMATHS::round(tree(1,2))+1;  // these will be the new rows
  tree(crow,2)=MISCMATHS::round(tree(1,2))+2;
  // add these new rows
  addrow(tree,newrow,MISCMATHS::round(tree(1,2))+1);
  addrow(tree,newrow,MISCMATHS::round(tree(1,2))+2);
  tree(1,2)+=2.0;  // number of valid rows
  // fill in the rest of the current node
  tree(crow,3) = disc_thr;
  tree.SubMatrix(crow,crow,4,3+nattr) = disc_vec;
  // recursively call this function and fill in the remaining two child nodes
  build_lda_tree(tree,rowslower,MISCMATHS::round(tree(crow,1)),tol);
  build_lda_tree(tree,rowsupper,MISCMATHS::round(tree(crow,2)),tol);
  return 0;
}


int build_lda_tree(Matrix& tree, const vector<int>& rows, float tol=0.01) 
{
  int retval;
  retval = build_lda_tree(tree,rows,0,tol);
  // tidy up tree by removing unnecessary rows
  Matrix tmp;
  tmp=tree;
  tree=tmp.SubMatrix(1,MISCMATHS::round(tree(1,2)),1,tree.Ncols());
  return retval;
}

int build_lda_tree(Matrix& tree, float tol=0.01) 
{
  vector<int> rows;
  rows.clear();
  for (int n=1; n<=trainingdata.Nrows(); n++) { rows.push_back(n); }
  return build_lda_tree(tree,rows,tol);
}

int initialise_training(const Matrix& data, const vector<int>& rows)
{
  // relies on globals for input and output!
  trainingdata=data;
  vector<int> sidx;
  Matrix tmpvec(rows.size(),1);
  nattr=trainingdata.Ncols()-1;
  sorted_indices.clear(); // ReSize(trainingdata.Nrows(),nattr);
  for (int fnum=1; fnum<=nattr; fnum++) {
    // get the sorted list of indices (store what rank each row is)
    // need to use rows to generate an appropriately sampled version of each column for the sort!
    for (int m=1; m<=tmpvec.Nrows(); m++) { tmpvec(m,1)=trainingdata(rows[m-1],fnum+1); }
    sidx = get_sortindex(tmpvec,"old2new",1);
    sorted_indices.push_back(sidx);
    // sorted_indices here store the ranks as a function of the indices of the *bagged* matrix 
    //  when used later: m = index of *current subset* matrix and rows[m] is index to *full bagged* matrix
    //  but sorted_indices *always* requires an input value as a row of the *full bagged* matrix
    // if (debug.value()) {
    //   if (fnum==1) { 
    // 	cout << "Initialise_training:: sidx = ";
    // 	for (int mj=0; mj<sidx.size(); mj++) cout << sidx[mj] << " ";
    // 	cout << endl;
    //   }
    // }
  }
  ranks.clear();
  for (int rk=1; rk<=trainingdata.Nrows(); rk++) {
    ranks.push_back(0);
  }
  return 0;
}

int initialise_testing(const Matrix& testdata)
{
  nattr=testdata.Ncols()-initcol;
  return 0;
}

////////////////////////////////////////////////////////////////////////////////////////

// Random Forest functions

// Need to be able to read and write a random forest to file
// Need to represent, for each tree, which features were used!
// Reshape each tree matrix into one covering all the features, but put zeros
//   wherever features were not used (this is fine for the LDA vecs to operate
//   on the full feature vecs then)
// Concatenate all tree matrices into one big Matrix and have the first N rows representing 
//   the offsets to get to the N different tree matrices (a bit wasteful but easy)

// Format for the random forest matrix:
//  1st row:  -3 | number of matrices (trees) | number of features | 0 | ... 
//  2nd to (n+1)th row:  -2 | row number of *this* matrix where tree matrix M can be found (M = this row -1) | 0 | 0 | ...
// (n+2)th - ... rows: 1st Tree Matrix (nothing changed internally, so all row references within are relative)
// .... rows: 2nd Tree Matrix
// .... rows: 3rd Tree Matrix
// ....
// .... rows: Last Tree Matrix

int rf_vec2mat(const vector<Matrix>& rforest, Matrix& rforest_mat)
{
  if (rforest.size()==0) return 0;
  size_t totrows=0;
  for (unsigned int n=0; n<rforest.size(); n++) {
    totrows+=rforest[n].Nrows();
  }
  int ncols = rforest[0].Ncols();
  rforest_mat.ReSize(rforest.size()+1+totrows,ncols);
  rforest_mat = 0.0;
  rforest_mat(1,1) = -3.0;
  rforest_mat(1,2) = rforest.size();
  rforest_mat(1,3) = trainingdata_full.Ncols()-initcol;
  int startrow=rforest.size() + 2;
  for (unsigned int n=0; n<rforest.size(); n++) {
    rforest_mat(2+n,1) = -2.0;
    rforest_mat(2+n,2) = startrow;
    rforest_mat.SubMatrix(startrow,startrow+rforest[n].Nrows()-1,1,ncols) = rforest[n];
    startrow += rforest[n].Nrows();
  }
  return 0;
}

int rf_mat2vec(const  Matrix& rfmat, vector<Matrix>& rforest)
{
  rforest.clear();
  if (rfmat.Nrows()<=0) { return 0; }
  if (fabs(rfmat(1,1)+3.0)>1e-6) { cerr << "WARNING: invalid matrix format for random forest" << endl; return 1; }
  int nmats=MISCMATHS::round(rfmat(1,2));
  int ncols=rfmat.Ncols(), nrows=rfmat.Nrows();
  Matrix tmp;
  int n1=0, n2=0;
  for (int n=0; n<nmats; n++) {
    if (fabs(rfmat(n+2,1)+2.0)>1e-6) { cerr << "WARNING: invalid matrix format for random forest" << endl; return 1; }
    n1 = MISCMATHS::round(rfmat(n+2,2));
    if (n<nmats-1) {
      n2 = MISCMATHS::round(rfmat(n+3,2));
    } else {
      n2=nrows+1;
    }
    tmp = rfmat.SubMatrix(n1,n2-1,1,ncols);
    if (fabs(tmp(1,1)+1.0)>1e-6) { cerr << "WARNING: invalid matrix format for random forest" << endl; return 1; }
    if (fabs(tmp(1,2)-tmp.Nrows())>1e-6) { cerr << "WARNING: invalid matrix format for random forest" << endl; return 1; }
    rforest.push_back(tmp);
  }
  return 0;
}

int save_random_forest(const vector<Matrix>& rforest, const string& filename)
{
  Matrix rfmat;
  rf_vec2mat(rforest,rfmat);
  return write_ascii_matrix(rfmat,filename);
}

int read_random_forest(vector<Matrix>& rforest, const string& filename)
{
  Matrix rfmat;
  rfmat = read_ascii_matrix(filename);
  rf_mat2vec(rfmat,rforest);
  return 0;
}

ColumnVector evaluate_random_forest(const vector<Matrix>& rforest, const Matrix& testdata) 
{
  int ntr=rforest.size();
  // precalculate the non-zero feature locations (for efficiency)
  if (debug.value()) { cout << "Starting evaluate_random_forest" << endl; }
  vector<ColumnVector> featureindices;
  featureindices.clear();
  for (int n=0; n<ntr; n++) {
    featureindices.push_back(precalc_nonzero_features(rforest[n]));
  }
  if (featureimportance.set() && (labelcol.set() || labelvol.set())) {
    featimp.ReSize(ntr,4);
    featimp=0.0;
    featnum.ReSize(ntr,nattr);
    featnum=0.0;
    for (int kk=0; kk<ntr; kk++) {
      for (int mm=1; mm<=featureindices[kk].Nrows(); mm++) {
	if (featureindices[kk](mm)>0.5) {
	  featnum(kk+1,MISCMATHS::round(featureindices[kk](mm))-3)=1.0;
	}
      }
    }
  }
  if (debug.value()) { cout << "Tree 215 precalc is " << featureindices[Min(featureindices.size()-1,(unsigned int)215)].t() << endl; }
  if (debug.value()) { cout << "  ... finished pre-calc" << endl; }
  ColumnVector res(testdata.Nrows());
  res=0.0;
  float retval=0.0;
  for (int m=1; m<=testdata.Nrows(); m++) {
    if (debug.value()) { cout << "  ... evaluating row " << m << endl; }
    int sum=0, goodtrees=0;
    for (int n=0; n<ntr; n++) {
      retval = evaluate_tree(rforest[n],featureindices[n],testdata,m);
      if (retval>=0.0) { sum += MISCMATHS::round(retval); goodtrees++; }  // ignore -1 returns
      if ((retval<0.9) && (retval>0.1)) { cerr << "WARNING: strange retval(" << retval << ") from evaluate_tree" << endl; }
      if (featureimportance.set() && (labelcol.set() || labelvol.set())) {
	// for each tree, store TP, FP, TN, FN stats over all datapoints (voxels)
	if (MISCMATHS::round(retval)==1) {  // negative reported
	  if (MISCMATHS::round(retval)==MISCMATHS::round(testdata(m,1))) {
	    featimp(n+1,3)+=1;  // true negative
	  } else {
	    featimp(n+1,4)+=1;  // false negative
	  }
	} else if (MISCMATHS::round(retval)==2) { // positive reported
	  if (MISCMATHS::round(retval)==MISCMATHS::round(testdata(m,1))) {
	    featimp(n+1,1)+=1;  // true positive
	  } else {
	    featimp(n+1,2)+=1;  // false positive
	  }
	}
      }
    }
    //res(m) = MISCMATHS::round(res(m)/ntr);  // majority voting for the binary case
    res(m) = ((float) sum)/goodtrees - 1.0;  // return the voting fraction \in [0,1]
  }
  return res;
}


int build_random_forest(vector<Matrix>& rforest, int ntrees=500)
{
  int nattr_full=nattr;
  int nvox=trainingdata.Nrows();
  int nf=global_nfeat;
  Matrix randsamp(nvox,nattr_full+1);
  Matrix attr_samp(nf,nattr_full);
  if (debug.value()) { cout << "Setup values: nf=" << nf << endl; } 
  if (debug.value()) { cout << "Setup values: nattr_full=" << nattr_full << endl; } 
  if (debug.value()) { cout << "Setup values: nvox=" << nvox << endl; } 
  if (debug.value()) { cout << "Setup values: ntrees=" << ntrees << endl; } 
  for (int n=1; n<=ntrees; n++) {
    if (debug.value()) { cout << endl << endl << "Building tree " << n << endl; } 
    // take a random set of input features
    attr_samp=0.0;
    int rr, steps;
    ColumnVector flags(nattr_full);
    flags=0.0;
    for (int aa=1; aa<=nf; aa++) {
      // generate unique list of attribute numbers by stepping along unflagged values
      steps = urand(1,nattr_full-aa+1);
      rr=0;
      while (steps>0) {
	rr++;
	if (flags(rr)<0.5) steps--;
      }
      if (debug.value()) { cerr << "aa=" << aa << " and rr=" << rr << endl; }
      attr_samp(aa,rr)=1.0;  //  use the rr'th attribute : rr in [1,nattr_full]
      flags(rr)=1.0;
    }

    if (debug.value()) { cout << "   ... selected random features" << endl; } 
    // Deal with Critical Features
    if (!ignore_critical_features.value()) {
      for (int mm=1; mm<=critical_features.Nrows(); mm++) {
	int x1, x2, x3;
	x1=MISCMATHS::round(critical_features(mm,1));  // the critical feature
	x2=MISCMATHS::round(critical_features(mm,2));  // min feature number of associated range
	x3=MISCMATHS::round(critical_features(mm,3));  // max feature number of associated range
	int count=0;
	if (flags(x1)<0.5) {  // only continue if the critical feature isn't already present
	  for (int cc=x2; cc<=x3; cc++) { if (flags(cc)>0.5) count++; }  // see if any associated features are there
	  if (count>=1) {
	    // pick one of the associated features to be eliminated
	    int rn=urand(1,count), elim=0;
	    // get the actual feature number (elim)
	    for (int cc=x2; cc<=x3; cc++) { if (flags(cc)>0.5) { rn--; if (rn==0) elim=cc; } }
	    if (debug.value()) { cout << "Swapping critical feature " << x1 << " for feature number " << elim << endl; }
	    flags(elim)=0.0;  flags(x1)=1.0;
	    // find the appropriate row in attr_samp
	    int aa=1;
	    while ((attr_samp(aa,elim)<0.5) && (aa<=attr_samp.Nrows())) { aa++; }
	    // aa should now be the row corresponding to the feature to be eliminated
	    attr_samp(aa,elim)=0.0;
	    attr_samp(aa,x1)=1.0;
	  }
	}
      }
    }

    if (debug.value()) { cout << "   ... selected attributes" << endl; } 
    int nrows=trainingdata_full.Nrows();
    Matrix newtrainingdata(nrows,nf+1); // normally substantially smaller than full data, so not too bad for memory
    newtrainingdata.SubMatrix(1,nrows,1,1)=trainingdata_full.SubMatrix(1,nrows,1,1);  // copy over labels
    newtrainingdata.SubMatrix(1,nrows,2,nf+1)=trainingdata_full.SubMatrix(1,nrows,2,nattr_full+1)*attr_samp.t();  // reduce features
    if (debug.value()) { cout << "   ... made reduced data" << endl; } 

    // Make a bagged random sample (sampling with repetition) from input
    Matrix randsamp(nrows,nf+1);
    if (nobagging.value()) {
      randsamp = newtrainingdata;
    } else {
      randsamp=0.0;
      for (int m=1; m<=newtrainingdata.Nrows(); m++) {
	int mrand=urand(1,nrows);
	randsamp.SubMatrix(m,m,1,nf+1)=newtrainingdata.SubMatrix(mrand,mrand,1,nf+1);
      }
    }
    // Start with rows set to 1 to N of the bagged sample (never point back to the original) NB: STARTS AT 1 NOT 0
    vector<int> rows;
    rows.clear();
    for (int m=1; m<=randsamp.Nrows(); m++) { rows.push_back(m); }
    if (debug.value()) { cout << "   ... made bagged sample" << endl; } 
    // Make this bagged sample the trainingdata matrix now
    initialise_training(randsamp,rows);

    Matrix newtree;
    build_lda_tree(newtree,rows,tolerance.value());
    if (debug.value()) { cout << "   ... built tree" << endl; } 
    // reshape output back into full attribute vector size to make everything consistent
    Matrix reshaped_newtree(newtree.Nrows(),nattr_full+3);
    if (debug.value()) { cout << "   ... reshaping tree" << endl; } 
    reshaped_newtree.SubMatrix(1,newtree.Nrows(),1,3) = newtree.SubMatrix(1,newtree.Nrows(),1,3);
    reshaped_newtree.SubMatrix(1,newtree.Nrows(),4,3+nattr_full) = newtree.SubMatrix(1,newtree.Nrows(),4,3+nf) * attr_samp;  // restore/reshape disc feature vectors
    rforest.push_back(reshaped_newtree);
  }
  return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

int crit_feat_num(int cf, const ColumnVector& featuresize) 
{
  //  feature number = sum of previous size of features per image + [5/1] depending on if the featuresize>0 or not
  int pos=0;
  for (int n=1; n<cf; n++) { if (featuresize(n)>0.5) { pos+=9; } else { pos++; } }
  if (featuresize(cf)>0.5) { pos+=5; } else { pos++; }
  return pos;
}


Matrix inputlist2mat(const string& filename)
{
  Matrix retmat;
  // open up file and extract list of individual filenames
  vector<string> fnames;
  fnames.clear();
  ifstream fs(filename.c_str());
  if (!fs) { 
    cerr << "Could not open file " << filename << endl;
    return retmat;
  }
  string cline, buf;
  vector<int> featuresizevec;
  vector<float> normvalvec;
  if (debug.value()) { cout << "Parsing input list file" << endl; }
  while (!fs.eof()) {
    getline(fs,cline);
    if (!fs.eof()) {
      stringstream ss(cline); // Insert the string into a stream
      if (debug.value()) { cout << "Current line: " << cline << endl; }
      ss >> buf;  // filename
      if (buf=="CRITICAL_FEATURE") {
	if (!ignore_critical_features.value()) {
	  // expecting row to be in form:  CRITICAL_FEATURE  c0  c1  c2
	  // where c0 = number of critical feature image, c1/c2 = start/end image numbers this applies to
	  // e.g.  "CRITICAL_FEATURE  1  1  3"  means the first image in the list (e.g. PD) contains the
	  //    critical feature (as the central one) and that this applies to any features taken from
	  //    anything in the images between 1 and 3 (inclusive)
	  int crit0, crit1, crit2;
	  ss >> buf;
	  crit0=atoi(buf.c_str());
	  ss >> buf;
	  crit1=atoi(buf.c_str());
	  ss >> buf;
	  crit2=atoi(buf.c_str());
	  Matrix newrow(1,3);
	  newrow << crit0 << crit1 << crit2;
	  if (critical_features.Nrows()==0) { 
	    critical_features = newrow; 
	  } else {
	    critical_features = critical_features & newrow;
	  }
	}
      } else {
	fnames.push_back(buf);
	ss >> buf; // featuresize (type = 0,1,3,9)
	featuresizevec.push_back(atoi(buf.c_str()));
	ss >> buf; // normalisation value 1
	normvalvec.push_back(atof(buf.c_str()));
	ss >> buf; // normalisation value 2
	normvalvec.push_back(atof(buf.c_str()));
      }
    }
  }
  fs.close();

  if (debug.value()) { cout << "Finished parsing input list file" << endl; }
  // go through each filename and read in image and process it appropriately
  volume4D<float> img;

  ColumnVector featuresize(featuresizevec.size());
  for (unsigned int mm=1; mm<=featuresizevec.size(); mm++) { featuresize(mm)=featuresizevec[mm-1]; }
  //  featuresize << 1 << 3 << 9 << 1 << 3 << 9 << 1 << 3 << 9 // PD and T2w and T1w
  //	      << 1 << 3 << 9 << 1 << 3 << 9 << 1 << 3 << 9 // Symm for PD and T2w then PseudoFLAIR
  //	      << 0 << 0 << 0;  // coords
  if (debug.value()) { cout << "featuresize vector = " << featuresize << endl; }



  Matrix normvals(normvalvec.size()/2,2);
  for (int mm=1; mm<=normvals.Nrows(); mm++) { 
    normvals(mm,1)=normvalvec[(mm-1)*2]; 
    normvals(mm,2)=normvalvec[(mm-1)*2+1]; 
  }
  if (debug.value()) { cout << "Normvals matrix size (from file) is: " << normvals.Nrows() << " by " << normvals.Ncols() << endl; }

  ColumnVector normsize(normvals.Nrows());
  for (int mm=1; mm<=normvals.Nrows(); mm++) { if (featuresize(mm)>0.5) normsize(mm)=9; else normsize(mm)=1; }
  //normsize << 27 << 27 << 27 << 27 << 27 << 27 << 1 << 1 << 1;  // number of features each normval is used for

  // readjust critical feature numbers to now be column numbers of the individual features
  for (int mm=1; mm<=critical_features.Nrows(); mm++) {
    for (int cc=1; cc<=3; cc++) {
      critical_features(mm,cc)=crit_feat_num(MISCMATHS::round(critical_features(mm,cc)),featuresize);
    }
  }

  if (debug.value()) { cout << "Feature sizes = " << featuresize.t() << endl; }
  int nfeatures=0;
  for (int mm=1; mm<=featuresize.Nrows(); mm++) { if (featuresize(mm)<0.5) nfeatures++; else nfeatures+=9; }
  if (debug.value()) { cout << "Number of features = " << nfeatures << endl; }

  // create a set of normvals 
  Matrix normvals_all(nfeatures,2);
  if (normvals.Ncols() > normvals.Nrows()) { normvals=normvals.t(); } // transpose if it is the wrong way around
  if (normvals.Ncols()<=0) { normvals_all=0.0; }  // defaults
  else if ((normvals.Nrows()==nfeatures) && (normvals.Ncols()==2)) { normvals_all=normvals; } // full set input
  else if ((normvals.Nrows()==normsize.Nrows()) && (normvals.Ncols()==2)) { // reduced set - unpack into a full set
    int c=0, cset=1;
    for (int mm=1; mm<=nfeatures; mm++) {
      c++;
      if (c>normsize(cset)+0.5) { cset++; c=1; }
      normvals_all(mm,1)=Min(normvals(cset,1),normvals(cset,2));
      normvals_all(mm,2)=Max(normvals(cset,1),normvals(cset,2));
      if (normvals_all(mm,2)==normvals_all(mm,1)) { normvals_all(mm,2)=1.0; normvals_all(mm,1)=0.0; }
    }
  } else {
    cerr << "Input normalisation value matrix the wrong size" << endl;
    exit(EXIT_FAILURE);
  }
  if (debug.value()) { cout << "Normalisation values are: " << endl << normvals_all << endl; }

  int sz=0, col=1, volnum=1;
  for (unsigned int n=0; n<fnames.size(); n++) {
    read_volume4D(img,fnames[n]);
    if (verbose.value()) { 
      cout << "Reading image " << fnames[n] << endl; 
      print_volume_info(img,fnames[n]); 
    }
    if (n==0) {
      // set up mask and resize retmat based on first image dimensions (and mask)
      mask=img[0];
      mask.binarise(0.5);
      long int nvox=MISCMATHS::round(mask.sum());
      if (debug.value()) { cerr << "non-zero voxels = " << nvox << endl; }
      retmat.ReSize(nvox,nfeatures);
    }
    for (int t=img.mint(); t<=img.maxt(); t++) {
      // decide whether to take 3x3 neighbourhood values, or extract 9 values from 3x3 or 9x9, or use values (e.g. coords)
      // use variable sz to indicate this: sz=0 (use values), sz=1 (use 3x3), sz=3 (use 9 values space 3 apart), sz=9
      int row=1, c=col;
      float retval=0.0;
      sz = MISCMATHS::round(featuresize(volnum++));
      for (int z=img.minz(); z<=img.maxz(); z++) {
	for (int y=img.miny(); y<=img.maxy(); y++) {
	  for (int x=img.minx(); x<=img.maxx(); x++) {
	    c=col;
	    if (mask(x,y,z)>0.5) {
	      for (int yo=-sz; yo<=sz; yo+=Max(sz,1)) {
		for (int xo=-sz; xo<=sz; xo+=Max(sz,1)) {
		  if (mask(x+xo,y+yo,z)>0.5) {
		    retval=img(x+xo,y+yo,z,t);
		  } else {
		    retval=img(x,y,z,t);
		  }
		  // normalise the values
		  retmat(row,c)=(retval-normvals_all(c,1))/(normvals_all(c,2)-normvals_all(c,1));
		  c++;
		}
	      }
	      row++;  // different row for each voxel
	    }
	  }
	}
      }
      if (sz>0) { col+=9; } else { col+=1; }
    } // t loop
  } // n loop
  if (debug.value()) { cerr << "Retmat start (2 rows) is: " << endl << retmat.SubMatrix(1,2,1,nfeatures) << endl; }
  return retmat;
}

ColumnVector reshape_img2vec(const volume<float>& img)
{
  ColumnVector res(MISCMATHS::round(mask.sum()));
  int row=1;
  for (int z=img.minz(); z<=img.maxz(); z++) {
    for (int y=img.miny(); y<=img.maxy(); y++) {
      for (int x=img.minx(); x<=img.maxx(); x++) {
	if (mask(x,y,z)>0.5) {
	  res(row++)=img(x,y,z);
	}
      }
    }
  }
  return res;
}


int write_mat2image(const string& fname, const Matrix& classmat)
{
  if (mask.nvoxels()<=0) { cerr << "ERROR: in write_mat2image - cannot find pre-loaded mask" << endl; return -1; }
  volume<float> res(mask);
  res=0.0;
  int row=1;
  for (int z=mask.minz(); z<=mask.maxz(); z++) {
    for (int y=mask.miny(); y<=mask.maxy(); y++) {
      for (int x=mask.minx(); x<=mask.maxx(); x++) {
	if (mask(x,y,z)>0.5) {
	  res(x,y,z)=classmat(row++,1);
	}
      }
    }
  }
  save_volume(res,fname);
  return 0;
}

int normalise_labels(ColumnVector& lbl)
{
  // set the range of labels [1,2]  (maybe allow zeros in there?!?)
  if ((lbl.Maximum()<1.5)  || (lbl.Maximum()>2.5))  lbl+=(2.0-lbl.Maximum());
  int retval=0;
  if ((lbl.Minimum()<0.5) || (lbl.Minimum()>1.5)) retval=1;
  return retval;
}

int separate_labels(Matrix& fullmat, ColumnVector& labelvec, int col)
{
  if ((col<=0) || (col>fullmat.Ncols())) return 0;
  // remove column number "col" and put these values into labels
  labelvec = fullmat.Column(col);
  normalise_labels(labelvec);
  Matrix mat(fullmat.Nrows(),fullmat.Ncols()-1);
  if (col>1) {
    mat.SubMatrix(1,fullmat.Nrows(),1,col-1) = fullmat.SubMatrix(1,fullmat.Nrows(),1,col-1);
  }
  if (col<fullmat.Ncols()) {
    mat.SubMatrix(1,fullmat.Nrows(),col,fullmat.Ncols()-1) = fullmat.SubMatrix(1,fullmat.Nrows(),col+1,fullmat.Ncols());
  }
  fullmat=mat;
  return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////


  // read in appropriate input volumes and build matrix of voxels by attributes
  // read_and_generate_data


int do_work(int argc, char* argv[]) 
{

  bool dotest=false, dotrain=false;
  if (load_rf.set() && (testdataname.set() || inputlist.set())) {
    dotest=true;
  }
  if (trainingdataname.set()) {
    dotrain=true;
    if (labelvol.unset() && labelcol.unset()) { 
      cerr << "Must specify a label volume or column to train a random forest" << endl; 
      return 1;
    }
  }
  
  vector<Matrix> rforest;
    
  if (dotrain) {
    // TRAINING ///////////////////
    if (verbose.value()) { cout << "Training phase" << endl; }
    if (seed.set()) srandom(seed.value());
    // read in appropriate data (might be for training or test - will be relabeled later if necessary)
    trainingdata_full = read_ascii_matrix(trainingdataname.value());
    if (verbose.value()) { cerr << "Training matrix read in: size = " << trainingdata_full.Nrows() << " by " << trainingdata_full.Ncols() << endl; }
    if (labelcol.unset() && labelvol.unset()) { initcol=0; }
    if (nfeat.value()==0) { global_nfeat=MISCMATHS::round(sqrt(trainingdata_full.Ncols()-initcol)); } else { global_nfeat=nfeat.value(); }
    
    vector<int> rows;  // used to index rows of the big trainingdata matrix
    rows.clear();
    for (int n=1; n<=trainingdata_full.Nrows(); n++) { rows.push_back(n); }
    // setup initial sorted vectors for the trainingdata (not needed for testing only)
    initialise_training(trainingdata_full,rows);
      
    if (criticalfeaturemat.set()) {
      critical_features = read_ascii_matrix(criticalfeaturemat.value()); 
    }

    cout << "MAKING RANDOM FOREST" << endl;
    build_random_forest(rforest, n_trees.value());

    save_random_forest(rforest,outname.value());
  }
  
  if (dotest) {
    // TESTING ///////////////////
    if (verbose.value()) { cout << "Testing phase" << endl; }
    if (labelcol.unset() && labelvol.unset()) { initcol=0; }
    
    read_random_forest(rforest,load_rf.value());

    // Load the trainingdata_full (if not already done in training phase above)
    if (testdataname.set()) { 
      trainingdata_full = read_ascii_matrix(testdataname.value()); 
    }    
    if (inputlist.set()) {
      trainingdata_full = inputlist2mat(inputlist.value());
    }
    if (labelvol.set()) {
      volume<float> labelv;
      read_volume(labelv,labelvol.value());
      ColumnVector labels_full;
      Matrix newdata;
      labels_full=reshape_img2vec(labelv);
      normalise_labels(labels_full);
      newdata = labels_full | trainingdata_full;
      trainingdata_full = newdata;
      initcol=1;
    }
    // The following creates a new reference to the trainingdata (TEMPORARY MEASURE... kind of...)
    Matrix& testdata = trainingdata_full;
    initialise_testing(testdata);
    if (nattr!=rforest[0].Ncols()-3) {  // sanity check on size
      cerr << "ERROR:: mismatch in number of features in data and random forest" << endl; 
      cerr << "  - check if --labelcol is correctly set" << endl; 
      exit(EXIT_FAILURE);
    }

    ColumnVector res;
    res = evaluate_random_forest(rforest,testdata);
    
    if (labelcol.set() || labelvol.set()) {
      int correct=0, fpos=0, fneg=0;
      for (int n=1; n<=testdata.Nrows(); n++) {
	if (fabs(res(n)-(testdata(n,1)-1))<0.5) correct++;
	if ((fabs(testdata(n,1)-1)<0.5) && (fabs(res(n)-1)<0.5)) fpos++;
	if ((fabs(testdata(n,1)-2)<0.5) && (fabs(res(n)-0)<0.5)) fneg++;
      }
      // cout << endl << endl << "RESULTS OF RFOREST EVALUATION (SAME DATA) :: " << endl << res.t() << endl << testdata.Column(1).t() << endl;
      cout << endl << endl << "RESULTS OF RFOREST EVALUATION = " << correct << " out of " << testdata.Nrows() << endl;
      cout << "False POS = " << fpos << " and False NEG = " << fneg << endl;
    }
    if (inputlist.set()) {  // write out image data
      write_mat2image(outname.value(),res);
    } else {
      write_ascii_matrix(res,outname.value());
    }
  }

  if (featureimportance.set()) {
    Matrix fimpres=featimp | featnum;
    write_ascii_matrix(fimpres,featureimportance.value());
  }
  
  return 0;
}

////////////////////////////////////////////////////////////////////////////

int main(int argc,char *argv[])
{

  Tracer tr("main");
  OptionParser options(title, examples);

  try {
    // must include all wanted options here (the order determines how
    //  the help message is printed)
    options.add(trainingdataname);
    options.add(testdataname);
    options.add(inputlist);
    options.add(outname);
    options.add(load_rf);
    options.add(nfeat);
    options.add(n_trees);
    options.add(labelcol);
    options.add(labelvol);
    options.add(nobagging);
    options.add(uselda);
    options.add(tolerance);
    options.add(maxtreesize);
    options.add(featureimportance);
    options.add(seed);
    options.add(criticalfeaturemat);
    options.add(ignore_critical_features);
    options.add(verbose);
    options.add(debug);
    options.add(help);
    
    nonoptarg = options.parse_command_line(argc, argv);

    // line below stops the program if the help was requested or 
    //  a compulsory option was not set
    if ( (help.value()) || (!options.check_compulsory_arguments(true)) )
      {
	options.usage();
	exit(EXIT_FAILURE);
      }
    

  // Call the local functions

  return do_work(argc,argv);
  }  catch(X_OptionError& e) {
    options.usage();
    cerr << endl << e.what() << endl;
    exit(EXIT_FAILURE);
  } catch(std::exception &e) {
    cerr << e.what() << endl;
  } 
}


