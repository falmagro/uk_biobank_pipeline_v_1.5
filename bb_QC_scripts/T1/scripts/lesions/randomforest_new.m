ntrees=500;
% fraction of training dataset size to use to train each tree (but randomly sampled so will probably leave some out)
fboot=1.0;   
% number of features used in each tree is the square root of the total features (default behaviour for classification)

% TRAIN
b = TreeBagger(ntrees,datavalues,classificationvalues,'oobpred','on','fboot',0.75);
cb=compact(b);                                                           
% oobError(b) is the plot of classification error (out of bag estimate) versus number of trees

% PREDICT
yfit=predict(cb,newdatavalues);                                                   
yf=str2num(char(yfit(:)));                                               
% length(find(yf~=newclassificationvalues)) is the number of mis-classifications on the new (test) data


