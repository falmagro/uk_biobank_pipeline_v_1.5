#!/usr/bin/env bash

source /fbp/bb_pipeline_v_2.5/init_vars

. $BB_BIN_DIR/bb_pipeline_tools/bb_set_header

set +x

# Update the pipeline with the latest version
pushd $BB_BIN_DIR;
git pull;
popd;

export SUBJID="$1"
export VISIT="$2"

if [ "$VISIT" == "" ] ; then
    export VISIT="2";
fi

#Line(s) to copy the zip files with the DICOMs into a valid structure inside /ukbdata/
#
# <SUBJID>/DICOM/16/<SUBJID>_20216_<VISIT>_0.zip
# <SUBJID>/DICOM/17/<SUBJID>_20217_<VISIT>_0.zip
# <SUBJID>/DICOM/18/<SUBJID>_20218_<VISIT>_0.zip
# <SUBJID>/DICOM/19/<SUBJID>_20219_<VISIT>_0.zip
# <SUBJID>/DICOM/20/<SUBJID>_20220_<VISIT>_0.zip
# <SUBJID>/DICOM/25/<SUBJID>_20225_<VISIT>_0.zip
# <SUBJID>/DICOM/66/<SUBJID>_20266_<VISIT>_0.zip
#

cd /ukbdata/;

mkdir $SUBJID/logs/
$BB_BIN_DIR/bb_autodownload/bb_convert_no_ram_disk $SUBJID $VISIT &> $SUBJID/logs/conv.txt

$BB_BIN_DIR//bb_pipeline_tools/bb_pipeline_queue.py $SUBJID -X 1

. $BB_BIN_DIR/bb_pipeline_tools/bb_set_footer
