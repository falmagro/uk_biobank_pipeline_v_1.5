FIXVERSION=1.06
#   (actually this version is 1.063 - see wiki page for details)

export FSL_FIX_OS=`uname -s`
export FSL_FIX_ARCH=`uname -m`

export FSL_FIXDIR=$BB_BIN_DIR/bb_functional_pipeline/bb_fix_dir/

export FSL_FIX_MCRROOT=$MCRROOT
export FSL_FIX_MCRV=`cat ${FSL_FIXDIR}/MCR.version`
export FSL_FIX_MCR=/mcr/v83/

export FSL_FIX_MLCDIR=${FSL_FIXDIR}/compiled/${FSL_FIX_OS}/${FSL_FIX_ARCH}
export FSL_FIX_MLOPTS="-nojvm -nodisplay -nodesktop -nosplash"

export FSL_FIX_MLEVAL="-r"
export FSL_FIX_MLFILE="\<"

export FSL_FIX_OCTAVE=/usr/bin/octave
export FSL_FIX_OCOPTS="--traditional -q --no-window-system"
export FSL_FIX_OCEVAL="--eval"
export FSL_FIX_OCFILE=""

export FSL_FIX_MATLAB_MODE=0

export FSL_FIX_CIFTIRW="$BB_BIN_DIR/bb_ext_tools/workbench/CIFTIMatlabReaderWriter";
export FSL_FIX_WBC="$BB_BIN_DIR/bb_ext_tools/workbench//bin_linux64/wb_command";

export FSL_FIX_CIFTIRW FSL_FIX_WBC

export FSL_FIX_FSLMATLAB=${FSLDIR}/etc/matlab
