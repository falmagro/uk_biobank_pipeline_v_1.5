#!/bin/env python

'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 15-Dec-2014 18:03:23$
 Version $1.0
 ProjectDir = 
'''

import sys
import argparse
import bb_pipeline_tools.bb_file_manager as FM
import bb_pipeline_tools.bb_logging_tool as LT

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def bb_pipeline_func(subject, jobHold, fileConfiguration, queue, coeff):

    logger  = LT.initLogging(__file__, subject)
    logDir  = logger.logDir
    baseDir = logDir[0:logDir.rfind('/logs/')]

    if not queue == "normal": 
        short_queues = ','.join(['short.qc@@' + x for x in queue])
        long_queues  = ','.join(['long.qc@@'  + x for x in queue])
    else:
        short_queues = 'short.qc@@short.hge'
        long_queues  = 'long.qc@@long.hge'

    jobsToWaitFor = ""

    jobPOSTPROCESS = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + short_queues + ' -N "bb_postprocess_struct_' + subject + '" -l ' + logDir + ' -j ' + str(jobHold) + ' $BB_BIN_DIR/bb_functional_pipeline/bb_postprocess_struct ' + subject )

    #TODO: Embed the checking of the fieldmap inside the independent steps -- Every step should check if the previous one has ended.
    if ('rfMRI' in fileConfiguration) and (fileConfiguration['rfMRI'] != ''):

        jobPREPARE_R = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + short_queues + ' -N "bb_prepare_rfMRI_'  + subject + '"  -l ' + logDir + ' -j ' + jobPOSTPROCESS + ' $BB_BIN_DIR/bb_functional_pipeline/bb_prepare_rfMRI ' + subject + ' ' + coeff )    
        jobFEAT_R =    LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues + '  -N "bb_feat_rfMRI_ns_'  + subject + '"  -l ' + logDir + ' -j ' + jobPREPARE_R   + ' ${FSLDIR}/bin/feat  ' + baseDir + '/fMRI/rfMRI.fsf' )
        jobFIX =       LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues + '  -N "bb_fix_'            + subject + '"  -l ' + logDir + ' -j ' + jobFEAT_R      + ' $BB_BIN_DIR/bb_functional_pipeline/bb_fix ' + subject )
        jobDR  =       LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues + '  -N "bb_ICA_dr_'         + subject + '"  -l ' + logDir + ' -j ' + jobFIX         + ' $BB_BIN_DIR/bb_functional_pipeline/bb_ICA_dual_regression ' + subject )
        jobCLEAN =     LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + short_queues + ' -N "bb_rfMRI_clean_'    + subject + '"  -l ' + logDir + ' -j ' + jobDR          + ' $BB_BIN_DIR/bb_functional_pipeline/bb_clean_fix_logs ' + subject )

        jobsToWaitFor  = jobCLEAN

    else:
        logger.error("There is no rFMRI info. Thus, the Resting State part will not be run")

    if ('tfMRI' in fileConfiguration) and (fileConfiguration['tfMRI'] != ''):
        jobPREPARE_T = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + short_queues + ' -N "bb_prepare_tfMRI_' + subject + '" -l ' + logDir + ' -j ' + jobPOSTPROCESS + ' $BB_BIN_DIR/bb_functional_pipeline/bb_prepare_tfMRI ' + subject + ' ' + coeff)   
        jobFEAT_T =    LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues +  ' -N "bb_feat_tfMRI_'    + subject + '" -l ' + logDir + ' -j ' + jobPREPARE_T   + ' ${FSLDIR}/bin/feat  ' + baseDir + '/fMRI/tfMRI.fsf' )

        if jobsToWaitFor != "":
            jobsToWaitFor = jobsToWaitFor +',' +jobFEAT_T
        else:
            jobsToWaitFor = '' + jobFEAT_T

    else:
        logger.error("There is no tFMRI info. Thus, the Task Functional part will not be run")

    if jobsToWaitFor=="":
        jobsToWaitFor="-1"

    return jobsToWaitFor

def main(): 
    parser = MyParser(description='BioBank Functional Pipeline')
    parser.add_argument("subjectFolder", help='Subject Folder')
    parser.add_argument("queue", help='queue_modifier', nargs="?", default="normal")
    parser.add_argument("jobID", help='jobID to hold for', nargs="?", default="-1")

    argsa = parser.parse_args()

    subject = argsa.subjectFolder
    subject = subject.strip()

    # If subject name ends with / --> remove it
    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]

    # Default queue scheme is "normal"
    if argsa.queue == []:
        queue = "normal"
    elif argsa.queue == 'normal':
        queue = "normal"
    else:
        queue = argsa.queue.split(",")

    # If no specified job ID to wait for wait for -1 (do not wait)
    if argsa.jobID == []:
        jobID = "-1"
    else:
        jobID = argsa.jobID

    logger = LT.initLogging(__file__, subject)

    logger.info('Running file manager') 
    fileConfig = FM.bb_file_manager(subject, False, False)

    logger.info("File configuration after running file manager: " + str(fileConfig))

    jobID2 = bb_pipeline_func(subject, jobID, fileConfig, queue)

    LT.finishLogging(logger)
    print(jobID2)
             
if __name__ == "__main__":
    main()
