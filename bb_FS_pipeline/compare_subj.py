#!/bin/env python

import sys
import argparse
import numpy as np

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main(): 

    parser = MyParser(description='Creates a mask with the number of voxels across the 4D image that have either (0 or negative values) or (NaN)')
    parser.add_argument('-f', dest="file",  type=str, nargs=1, help='Input file')
    parser.add_argument('-s1', dest="s1", type=str, nargs=1, help='Subject 1')
    parser.add_argument('-s2', dest="s2", type=str, nargs=1, help='Subject 2')

    argsa = parser.parse_args()

    if (argsa.file==None):
        parser.print_help()
        exit()

    if (argsa.s1==None):
        parser.print_help()
        exit()

    if (argsa.s2==None):
        parser.print_help()
        exit()
        
    fileName=argsa.file[0]

    d={}
    f=open(fileName)
    
    # Ignore first line
    f.readline().split(" ")
    
    for line in f:
        tok=line.split()
        d[tok[0]]=np.array([float(x) for x in tok[1:]])

    data_s1=d[argsa.s1[0]]
    data_s2=d[argsa.s2[0]]
   
    
    result=np.corrcoef(data_s1,data_s2)

    print(result[0, 1])
    
if __name__ == "__main__":
    main()


