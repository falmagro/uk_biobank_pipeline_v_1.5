#!/bin/env python
'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 $15-Dec-2014 10:41:10$
 Version $1.0
 ProjectDir = 
 '''

import os
import glob
import sys,argparse,os.path
import bb_general_tools.bb_path as bb_path

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def bb_correct_image_names(folder): 
    
    possibleExtensions=['.nii', '.nii.gz', '.bval', '.bvec', '.json']

    listFiles=glob.glob(folder + '/*.*')

    for fullFileName in listFiles:

        try:
            extension=bb_path.getExt(fullFileName,possibleExtensions)
            fileName=bb_path.removeExt(fullFileName,possibleExtensions)
        except ValueError:
            continue        

        parts=fileName.split('_')
        
        for i,part in enumerate(parts):
            try:
                #intPart=int(part)
                parts[i]=part.lstrip('0')
        
            except ValueError:
                pass

        newFullFileName="_".join(parts)+ extension
        if newFullFileName != fullFileName:
            print("Moving " + fullFileName + " to " + newFullFileName)
            os.rename(fullFileName,newFullFileName)
 
def main():
    parser = MyParser(description='BioBank Tool to correct a problem from files coming out of dcm2niix')
    parser.add_argument('folder', help='Folder to process')

    argsa = parser.parse_args()

    folder = argsa.folder
    folder = folder.strip()

    bb_correct_image_names(folder)

if __name__ == "__main__":
    main()


