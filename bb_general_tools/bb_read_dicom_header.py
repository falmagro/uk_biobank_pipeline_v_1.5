#!/bin/env python
'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 $15-Dec-2014 10:41:10$
 Version $1.0
 ProjectDir = 
 '''

import os
import pydicom
import sys,argparse,os.path


class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main(): 
    
    parser = MyParser(description='BioBank Dicom Header Reader')
    parser.add_argument('-f', dest="file", type=str, nargs=1, help='Read dicom file')
    parser.add_argument('--all', dest='allFields', action='store_true')

    allFields = False

    argsa = parser.parse_args()
    
    if (argsa.file==None):
        parser.print_help()
        exit()

    if (argsa.allFields==True):
        allFields=True

    fileName = argsa.file[0]

    ds = pydicom.dcmread(fileName, stop_before_pixels=True)
    
    if ( not allFields ):
        excludedFilesListFileName=os.environ['BB_BIN_DIR']+"/bb_data/dicom_fields_to_exclude.txt"
        
        with open(excludedFilesListFileName) as f:
            hexKeys=f.readlines()
        for hexKey in hexKeys:
            keys=[int(x,0) for x in hexKey.split()]
            ds[keys].value=''
    try:
        print(ds)
    except TypeError:
        ds.remove_private_tags()
        print(ds)
   
if __name__ == "__main__":
    main()


