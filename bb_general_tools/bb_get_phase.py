#!/bin/env python
'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 $15-Dec-2014 10:41:10$
 Version $1.0
 ProjectDir = 
 '''
import glob
import json
import sys,argparse,os.path
from bb_read_json_field import bb_read_json_field


class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(): 
    
    parser = MyParser(description='BioBank Phase Reader')
    parser.add_argument("-s" , dest="subjectFolder",type=str, help='Subject Folder')
    parser.add_argument('-d', dest="date", type=str, nargs=1, help='Acquistion Date of the subject')

    argsa = parser.parse_args()

    subject = argsa.subjectFolder
    
    if not subject == None: 
        subject = subject.strip()

    if (argsa.date==None):

        fileDir=subject + "/T1"

        fileDir=subject + "/T1"

        if os.path.isdir(fileDir+ "/unusable"):
            fileDir = fileDir+ "/unusable"

        fileName=fileDir + "/T1.json"

        if not os.path.isfile(fileName):
            foundFile=glob.glob(fileDir + "/*/T1.json")
            if foundFile == [] :
                foundFile=glob.glob(fileDir + "/*/T1.json")
                
                print('-1')            
                exit(1)
            else:
                fileName=foundFile[0]

        res=bb_read_json_field(fileName, "AcquisitionDate")

        if res==[]:
            print('-1')
            exit(1)
        else:
            res=float(res)
    else:
        res=int(argsa.date[0])

    with open(os.environ['BB_BIN_DIR']+'/bb_data/phases_dates.json') as data_file:
        phases_dates=json.load(data_file)
  
    for key in phases_dates.keys():
        if (phases_dates[key][0]<=res) and (res <=phases_dates[key][1]):
            print(key)
            exit(0)

    print('-1')

if __name__ == "__main__":
    main()


