#!/bin/env python

'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 15-Dec-2014 18:03:23$
 Version $1.0
 ProjectDir = 
'''

import os,sys,argparse
import bb_pipeline_tools.bb_logging_tool as LT
import bb_pipeline_tools.bb_file_manager as FM

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def bb_pipeline_asl(subject, jobHold, fileConfiguration):

    logger = LT.initLogging(__file__, subject)
    logDir  = logger.logDir
    baseDir = logDir[0:logDir.rfind('/logs/')]

    jobASL_1 = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q short.qc@@short.hge  -N "bb_asl_rename_'   + subject + '" -j ' + str(jobHold)  + ' -l ' + logDir + ' $BB_BIN_DIR/bb_asl_pipeline/bb_asl_rename '   + subject )
    jobASL_2 = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q short.qc@@short.hge  -N "bb_asl_proc_'     + subject + '" -j ' + str(jobASL_1) + ' -l ' + logDir + ' $BB_BIN_DIR/bb_asl_pipeline/bb_asl_proc '     + subject )
    jobASL_3 = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q short.qc@@short.hge  -N "bb_asl_get_IDPs_' + subject + '" -j ' + str(jobASL_2) + ' -l ' + logDir + ' $BB_BIN_DIR/bb_asl_pipeline/bb_asl_get_IDPs ' + subject )

    
    return jobASL_3

def main(): 
  
    parser = MyParser(description='BioBank ASL Tool')
    parser.add_argument("subjectFolder", help='Subject Folder')

    argsa = parser.parse_args()

    subject = argsa.subjectFolder
    subject = subject.strip()

    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]
    
    fileConfig = FM.bb_file_manager(subject)

    jobSTEP1 = bb_pipeline_asl(subject, '-1', fileConfig)
    
    print(jobSTEP1)
             
if __name__ == "__main__":
    main()
