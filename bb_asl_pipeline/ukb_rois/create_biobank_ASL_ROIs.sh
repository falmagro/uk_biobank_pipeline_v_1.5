#!/bin/sh

# T.O. Create vascular territory and other ROIs for Biobank ASL analysis

## ---- Vascular territories ---- ##
# Use Manus Donahue's vascular territory probability atlas. 
# Downloaded from https://www.vumc.org/donahue-lab/sites/default/files/public_files/flowterritories.zip on 10/3/2021
# First merge into one file
fslmerge -t All_Maps MNI_2MM_RICA.nii.gz MNI_2MM_LICA.nii.gz MNI_2MM_VBA.nii.gz 

# Find the index with the highest value
fslmaths All_Maps.nii.gz -Tmaxn -add 1 -mul $FSLDIR/data/standard/MNI152_T1_2mm_brain_mask MostProbableSupplier

# Threshold at 90% first
fslmaths All_Maps.nii.gz -thr 0.9 -Tmean -bin All_Maps_maskThr90
fslmaths All_Maps.nii.gz -thr 0.9 -bin -Tmaxn -add 1 -mul All_Maps_maskThr90 VesMasksThr90

# Repeat after smoothing first to get slightly more robust territories
fslmaths All_Maps.nii.gz -s 3 All_Maps_sm3
fslmaths All_Maps_sm3 -Tmaxn -add 1 -mul $FSLDIR/data/standard/MNI152_T1_2mm_brain_mask MostProbableSupplier_sm3

# Use a slightly lower threshold here (80%) to make sure the ROIs cover a reasonable volume
fslmaths All_Maps_sm3 -thr 0.8 -Tmean -bin All_Maps_sm3_maskThr80
fslmaths All_Maps_sm3 -thr 0.8 -bin -Tmaxn -add 1 -mul All_Maps_sm3_maskThr80 VesMasks_sm3Thr80

# These look pretty good. In the output background = 0, RICA = 1, LICA = 2 and VBA = 3

# Repeat with a greater degree of smoothing
#fslmaths All_Maps.nii.gz -s 5 All_Maps_sm5
#fslmaths All_Maps_sm5 -Tmaxn -add 1 -mul $FSLDIR/data/standard/MNI152_T1_2mm_brain_mask MostProbableSupplier_sm5


## ---- WM ROIs ---- ##
fslroi $FSLDIR/data/atlases/HarvardOxford/HarvardOxford-sub-prob-2mm.nii.gz HO_L_Cerebral_WM_prob 0 1
fslmaths HO_L_Cerebral_WM_prob -thr 80 -bin HO_L_Cerebral_WM_thr80

fslroi $FSLDIR/data/atlases/HarvardOxford/HarvardOxford-sub-prob-2mm.nii.gz HO_R_Cerebral_WM_prob 11 1
fslmaths HO_R_Cerebral_WM_prob -thr 80 -bin HO_R_Cerebral_WM_thr80


## ---- MNI ROIs ---- ##
fslmaths $FSLDIR/data/atlases/MNI/MNI-prob-2mm.nii.gz -Tmax -bin MNI_seg_mask
fslmaths $FSLDIR/data/atlases/MNI/MNI-prob-2mm.nii.gz -Tmaxn -add 1 -mul MNI_seg_mask MNI_seg_max_prob_masked

# Need to split these into R and L hemispheres
fslmaths $FSLDIR/data/standard/MNI152_T1_2mm -mul 0 -add 1 -roi 0 45 0 -1 0 -1 0 -1 MNI_rightvol
fslmaths $FSLDIR/data/standard/MNI152_T1_2mm -mul 0 -add 1 -roi 46 45 0 -1 0 -1 0 -1 MNI_leftvol

fslmaths MNI_seg_max_prob_masked -mul MNI_rightvol MNI_seg_max_prob_masked_R
fslmaths MNI_seg_max_prob_masked -mul MNI_leftvol MNI_seg_max_prob_masked_L

fslmaths MNI_seg_max_prob_masked_L -add 9 -mul MNI_seg_max_prob_masked -add MNI_seg_max_prob_masked_R MNI_seg_max_prob_masked_RandL

# Show the labels
echo 'MNI Labels:'
echo '0: Background'
for (( Idx = 0; Idx<=8; Idx++ )); do 
    echo `echo $Idx +1 | bc`\: Right `grep index=\"$Idx /usr/local/fsl/data/atlases/MNI.xml | sed 's:</label>::'| sed 's:.*>::'`; 
done
for (( Idx = 0; Idx<=8; Idx++ )); do 
    echo `echo $Idx +10 | bc`\: Left `grep index=\"$Idx /usr/local/fsl/data/atlases/MNI.xml | sed 's:</label>::'| sed 's:.*>::'`; 
done
