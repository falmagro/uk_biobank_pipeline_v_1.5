#!/bin/env python

'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 15-Dec-2014 18:03:23$
 Version $1.0
 ProjectDir =
'''

import bb_pipeline_tools.bb_logging_tool as LT

def bb_pipeline_diff_joint(subject, jobHold, fileConfiguration, queue, coeff,
                           num_shells, b_value_shell, exec_block):

    logger = LT.initLogging(__file__, subject)
    logDir  = logger.logDir
    baseDir = logDir[0:logDir.rfind('/logs/')]

    if not queue == "normal":
        short_queues = ','.join(['short.qc@@' + x for x in queue])
        long_queues  = ','.join(['long.qc@@'  + x for x in queue])
    else:
        short_queues = 'short.qc@@short.hge'
        long_queues  = 'long.qc@@long.hge'


    # Run bedpostX with CPU or GPU
    if ((exec_block == 20) or (exec_block == 123)):
        eddy_cpu = True
    else:
        eddy_cpu = False

    if (exec_block == 40):
        bedpostx_cpu = True
    else:
        bedpostx_cpu = False

    # Run block 2 --> eddy
    if ((exec_block == 0) or (exec_block == 123) or (exec_block == 2) or (exec_block == 20)):
        run_block_2 = True
    else:
        run_block_2 = False

    # Run block 3 --> After eddy, before bedpostx
    if ((exec_block == 0) or (exec_block == 123) or (exec_block == 3)):
        run_block_3 = True
    else:
        run_block_3 = False

    # Run block 4 --> bedpostx + probtrack
    if ((exec_block == 0) or (exec_block == 4) or (exec_block == 40)):
        run_block_4 = True
    else:
        run_block_4 = False

    # This should never happen
    if ((exec_block == 1) or (exec_block == 5)):
        return 1

    jobEDDY="1"
    jobDTIFIT="1"
    jobTBSS="1"
    jobPREBEDPOSTX="1"
    jobNODDI="1"
    finalJob="1"

    if run_block_2:
        jobPREPARE  = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + short_queues + ' -N "bb_pre_eddy_' + subject + '" -j ' + str(jobHold) + '  -l ' + logDir + ' $BB_BIN_DIR/bb_diffusion_pipeline/bb_eddy/bb_pre_eddy ' + subject )
        if eddy_cpu:
            jobEDDY = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues  + ' -N "bb_eddy_cpu_' + subject + '" -j ' + jobPREPARE   + '  -l ' + logDir + ' $BB_BIN_DIR/bb_diffusion_pipeline/bb_eddy/bb_eddy_wrap_cpu ' + baseDir)
        else:
            jobEDDY = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues  + ' -N "bb_eddy_gpu_' + subject + '" -j ' + jobPREPARE   + '  -l ' + logDir + ' $BB_BIN_DIR/bb_diffusion_pipeline/bb_eddy/bb_eddy_wrap_gpu ' + baseDir)
        finalJob = finalJob + "," + str(jobEDDY)

    if run_block_3:
        jobPOSTEDDY    = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues  + ' -N "bb_post_eddy_'    + subject + '" -j ' + jobEDDY     + '  -l ' + logDir + ' $BB_BIN_DIR/bb_diffusion_pipeline/bb_eddy/bb_post_eddy ' + baseDir + ' ' + coeff + ' ' + str(b_value_shell))
        jobDTIFIT      = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + short_queues + ' -N "bb_dtifit_'       + subject + '" -j ' + jobPOSTEDDY + '  -l ' + logDir + ' ${FSLDIR}/bin/dtifit -k ' + baseDir + '/dMRI/dMRI/data_ud_1_shell -m ' + baseDir + '/dMRI/dMRI/nodif_brain_mask_ud -r ' + baseDir + '/dMRI/dMRI/data_ud_1_shell.bvec -b ' + baseDir + '/dMRI/dMRI/data_ud_1_shell.bval -o ' + baseDir + '/dMRI/dMRI/dti')
        jobTBSS        = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues  + ' -N "bb_tbss_'         + subject + '" -j ' + jobDTIFIT   + '  -l ' + logDir + ' $BB_BIN_DIR/bb_diffusion_pipeline/bb_tbss/bb_tbss_general ' + subject )
        if num_shells > 1:
            jobNODDI   = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues  + ' -N "bb_NODDI_'        + subject + '" -j ' + jobTBSS     + '  -l ' + logDir + ' $BB_BIN_DIR/bb_diffusion_pipeline/bb_NODDI ' + subject )
        jobPREBEDPOSTX = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + short_queues + ' -N "bb_pre_bedpostx_' + subject + '" -j ' + jobDTIFIT   + '  -l ' + logDir + ' $BB_BIN_DIR/bb_diffusion_pipeline/bb_bedpostx/bb_pre_bedpostx ' + baseDir + '/dMRI/dMRI')
        finalJob = finalJob + "," + str(jobPREBEDPOSTX)

    if run_block_4:
        if bedpostx_cpu:
            jobBEDPOSTX = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues  + ' -N "bb_bedpostx_cpu_'         + subject + '" -j ' + jobPREBEDPOSTX + '  -l ' + logDir + ' $BB_BIN_DIR/bb_diffusion_pipeline/bb_bedpostx/bb_bedpostx_cpu ' + baseDir + '/dMRI/dMRI')
            jobAUTOPTX  = LT.runCommand(logger, '${BB_BIN_DIR}/bb_diffusion_pipeline/bb_autoPtx/bb_autoPtx_queue_cpu_joint ' + subject + ' ' + jobBEDPOSTX + ',' + jobTBSS + ' ' + long_queues)

        else:
            jobBEDPOSTX = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues  + ' -N "bb_bedpostx_gpu_'         + subject + '" -j ' + jobPREBEDPOSTX + '  -l ' + logDir + ' $BB_BIN_DIR/bb_diffusion_pipeline/bb_bedpostx/bb_bedpostx_gpu ' + baseDir + '/dMRI/dMRI')
            jobAUTOPTX  = LT.runCommand(logger, '${BB_BIN_DIR}/bb_diffusion_pipeline/bb_autoPtx/bb_autoPtx_queue_gpu_joint ' + subject + ' ' + jobBEDPOSTX + ',' + jobTBSS + ' ' + long_queues)

        finalJob = finalJob + "," + str(jobAUTOPTX)

    return finalJob
