#!/bin/env python

import bb_pipeline_tools.bb_logging_tool as LT

def bb_IDP(subject, jobHold, fileConfiguration, queue):

    logger = LT.initLogging(__file__, subject)
    logDir  = logger.logDir

    if not queue == "normal": 
        long_queues  = ','.join(['long.qc@@'   + x for x in queue])
    else:
        #short_queues = 'short.qc@@short.hge'
        long_queues  = 'long.qc@@long.hge'

    jobIDP = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q ' + long_queues + '  -N "bb_IDP_' + subject + '" -j ' + str(jobHold) + '  -l ' + logDir + ' $BB_BIN_DIR/bb_IDP/bb_IDP ' + subject )
    return jobIDP
