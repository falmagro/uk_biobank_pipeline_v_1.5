#!/bin/env python
'''
 FMRIB, Oxford University
 $15-Dec-2014 10:41:10$
 Version $1.0
 ProjectDir =
 '''

import os
import sys
import os.path
import argparse
import bb_logging_tool as LT
from bb_file_manager                                    import bb_file_manager
from bb_basic_QC                                        import bb_basic_QC
from bb_structural_pipeline.bb_pipeline_struct_queue    import bb_pipeline_struct
from bb_functional_pipeline.bb_pipeline_func_queue      import bb_pipeline_func
from bb_diffusion_pipeline.bb_pipeline_diff_queue_joint import bb_pipeline_diff_joint
from bb_FS_pipeline.bb_pipeline_FS_queue                import bb_pipeline_FS
from bb_asl_pipeline.bb_pipeline_asl                    import bb_pipeline_asl
from bb_post_pipeline.bb_post_pipeline_queue            import bb_post_pipeline
from bb_IDP.bb_IDP_queue                                import bb_IDP
from bb_QSM_pipeline.bb_pipeline_QSM                    import bb_pipeline_QSM
from bb_surf_pipeline.bb_pipeline_surf                  import bb_pipeline_surf

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def choose_block(subject, runTopup, fileConfig, queue, coeff, num_shells,
                   b_value_shell, exec_block):

    if exec_block == 0:
        # Default value for job id. SGE does not wait for a job with this id.
        jobSTEP2 = "1"
        jobSTEP3 = "1"
        jobSTEP4 = "1"

        jobSTEP1 = bb_pipeline_struct(subject, runTopup, fileConfig, queue, coeff)
        jobQSM1  = bb_pipeline_QSM(subject, jobSTEP1, fileConfig)

        if runTopup:
             jobSTEP2 = bb_pipeline_func(subject, jobSTEP1, fileConfig, queue, coeff)
             jobSTEP3 = bb_pipeline_asl(subject, jobSTEP1, fileConfig)
             jobSTEP4 = bb_pipeline_diff_joint(subject, jobSTEP1, fileConfig, queue,
                                               coeff, num_shells, b_value_shell,
                                               exec_block)

        jobSTEP5 = bb_pipeline_FS(subject, jobSTEP1, fileConfig, queue)
        jobSTEP6 = bb_pipeline_surf(subject, str(jobSTEP2) + "," + str(jobSTEP5),
                                    fileConfig, queue)
        jobSTEP7 = bb_IDP(subject, str(jobSTEP1) + "," + str(jobSTEP2) + "," + str(jobSTEP4),
                          fileConfig, queue)
        jobSTEP8 = bb_post_pipeline(subject, str(jobSTEP7) + "," + str(jobSTEP5),
                                    fileConfig, queue)

        return jobSTEP8

    elif exec_block == 1:
        jobSTEP1 = bb_pipeline_struct(subject, runTopup, fileConfig, queue, coeff)
        jobQSM1  = bb_pipeline_QSM(subject, jobSTEP1, fileConfig)

        if runTopup:
             jobSTEP2 = bb_pipeline_func(subject, jobSTEP1, fileConfig, queue, coeff)
             jobSTEP3 = bb_pipeline_asl(subject, jobSTEP1, fileConfig)
             return jobSTEP3
        else:
            return jobSTEP1

    elif exec_block == 123:
        jobSTEP1 = bb_pipeline_struct(subject, runTopup, fileConfig, queue, coeff)
        jobQSM1  = bb_pipeline_QSM(subject, jobSTEP1, fileConfig)

        if runTopup:
             jobSTEP2 = bb_pipeline_func(subject, jobSTEP1, fileConfig, queue, coeff)
             jobSTEP3 = bb_pipeline_asl(subject, jobSTEP1, fileConfig)
             jobSTEP4 = bb_pipeline_diff_joint(subject, jobSTEP1, fileConfig, queue,
                                               coeff, num_shells, b_value_shell,
                                               exec_block)
             return jobSTEP4
        else:
            return jobSTEP1

    elif ((exec_block == 2) or (exec_block == 20) or (exec_block == 3) or
         (exec_block == 4) or (exec_block == 40)):
        if runTopup:
             jobSTEP3 = bb_pipeline_diff_joint(subject, 1, fileConfig, queue,
                                               coeff, num_shells, b_value_shell,
                                               exec_block)
             return jobSTEP3
        else:
             return 1

    if exec_block == 5:
        jobSTEP5 = bb_pipeline_FS(subject, 1, fileConfig, queue)
        jobSTEP6 = bb_IDP(subject, 1, fileConfig, queue)
        jobSTEP7 = bb_post_pipeline(subject, str(jobSTEP5) + "," + str(jobSTEP6), fileConfig, queue)

        return jobSTEP7

    if exec_block == 6:
        jobSTEP8 = bb_pipeline_surf(subject, 1, fileConfig, queue)
 
        return jobSTEP8

    else:
        return 1

def main():

    parser = MyParser(description='BioBank Pipeline Manager')
    parser.add_argument("subjectFolder", help='Subject Folder', action="store")
    parser.add_argument("-q", "--queue", help='Queue modifier (default: normal)',
                        action="store", nargs="?", dest="queue", default="normal")
    parser.add_argument("-n", "--normcheck", action="store_false", default=True,
                        help='Do NOT check Normalisation in structural image (default if flag not used: Check normalisation)',
                        dest="norm_check")
    parser.add_argument("-P", "--namingPatterns", action="store", nargs="?",
                        default=os.environ['BB_BIN_DIR'] + "/bb_data/naming_pattern_UKBB.json",
                        help='File with the naming patterns coming from dcm2niix (default: UKB naming patterns)',
                        dest="naming_patterns")
    parser.add_argument("-c", "--coeff", action="store", nargs="?",
                        default=os.environ['BB_BIN_DIR'] + "/bb_data/bb_GDC_coeff_skyra.grad",
                        help='Coefficient file for the GDC (Gradient Distiortion Correction). \n' +
                              '   Options:  \n' +
                              '           none--> No GDC is performed' +
                              '           skyra --> Uses the default Siemens Skyra gradients' +
                              '           prisma --> Uses the default Siemens Prisma gradients' +
                              '           <path to file> --> Uses the user-pecified gradients file' +
                              '            --> If this option is not used, the default is Siemens Skyra',
                        dest="coeff")
    parser.add_argument("-C", "--coils_SWI", action="store", nargs="?", default=32,
                        help='Number of coils for SWI data. Default: 32. 0 means "no separate coil data"',
                        dest="coils_SWI")
    parser.add_argument("-E", "--echoes_SWI", action="store", nargs="?", default=2,
                        help='Number of echo times for SWI data (default: 2)',
                        dest="echoes_SWI")
    parser.add_argument("-Q", "--basic_QC_file", action="store", nargs="?",
                        default=os.environ['BB_BIN_DIR'] + "/bb_data/ideal_config_sizes_UKBB.json",
                        help='File with the ideal configuration of the files (default: UKB)',
                        dest="basic_QC_file")
    parser.add_argument("-p", "--complex_phase", action="store_true", default=False,
                        help='SWI phase is complex (default: False)',
                        dest="complex_phase")
    parser.add_argument("-i", "--inverted_PED", action="store_true", default=False,
                        help='Use if the dMRI Phase Encoding Direction is inverted (default if flag not used: False)',
                        dest="inverted_PED")
    parser.add_argument("-b", "--b_value_shell", action="store", nargs="?",
                        default=1000,
                        help='B value for the single shell data to be used (default: 1000)',
                        dest="b_value_shell")
    parser.add_argument("-B", "--B_files", action="store", nargs="?",
                        default="",
                        help='Name of the directory with the bval and bvec files for dMRI data.\n' +
                             'If specified, the directory needs to have these 4 files: \n' +
                             '    - AP.bval\n ' +
                             '    - AP.bvec\n ' +
                             '    - PA.bval\n ' +
                             '    - PA.bvec\n ' +
                             'If not set, the pipeline will use the dcm2niix generated files',
                        dest="B_files")
    parser.add_argument("-S", "--num_shells", action="store", nargs="?", default=2,
                        help='Number of different shells (B-values) for dMRI data.',
                        dest="num_shells")
    parser.add_argument("-X", "--exec_block", action="store", nargs="?", default=0,
                        help='Pipeline block(s) to execute: Option (default 0): \n' +
                             '  - 0: Run the whole pipeline with default parameters \n' +
                             '  - 1: Run   (CPU) 1st block (up until eddy) \n' +
                             '  - 2: Run   (GPU) 2nd block (eddy) \n' +
                             '  - 3: Run   (CPU) 3rd block (after eddy, before bedpostX + probtrackX) \n' +
                             '  - 4: Run   (GPU) 4th block (bedpostX + probtrackX) \n' +
                             '  - 5: Run   (CPU) 5th block (everything after bedpostX + probtrackX) \n' +
                             '  - 20: Run  (CPU) 2nd block (eddy) \n '+
                             '  - 40: Run  (CPU) 4th block (bedpostX + probtrackX) \n ' +
                             '  - 123: Run (CPU) 1st, 2nd, & 3rd block (up until bedpostX) \n ',
                        dest="exec_block")

    #########################
    # ARGUMENT PARSING CODE #
    #########################
    argsa = parser.parse_args()

    # Subject argument
    subject = argsa.subjectFolder
    subject = subject.strip()
    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]

    # Setting up logging
    logger = LT.initLogging(__file__, subject)
    logger.info('Running file manager')

    # Parsing coefficient file argument
    if not argsa.coeff:
        coeff = "skyra"
    else:
        coeff = argsa.coeff
    if coeff == "":
        coeff = "skyra"
    if coeff not in ["skyra", "prisma", "none"]:
        if not os.path.exists(coeff):
            logger.error("ERROR: Subject cannot be run. Incorrect GDC file specified: " +
                        coeff)
            LT.finishLogging(logger)
            sys.exit(1)

    # Deciding Grafient coefficients file
    if coeff == "skyra":
        coeff = os.environ['BB_BIN_DIR'] + "/bb_data/bb_GDC_coeff_skyra.grad"
    elif coeff == "prisma":
        coeff = os.environ['BB_BIN_DIR'] + "/bb_data/bb_GDC_coeff_prisma.grad"

    if not os.path.exists(coeff):
        coeff = "none"

    # Parsing number of SWI coils argument
    if not argsa.coils_SWI:
        coils_SWI = 32
    elif argsa.coils_SWI == "":
        coils_SWI = 32
    else:
        try:
            coils_SWI = int(argsa.coils_SWI)
        except ValueError:
            logger.warning("ERROR in the specified number of SWI coils: " +
                            coils_SWI + '. Will use default: 32')
            coils_SWI = 32

    # Parsing number of SWI echo times argument
    if not argsa.echoes_SWI:
        echoes_SWI = 2
    elif argsa.echoes_SWI == "":
        echoes_SWI = 2
    else:
        try:
            echoes_SWI = int(argsa.echoes_SWI)
        except ValueError:
            logger.warning("ERROR in the specified number of shells: " +
                            echoes_SWI + '. Will use default: 2')
            echoes_SWI = 2

    # Parsing number of dMRI shells argument
    if not argsa.num_shells:
        num_shells = 2
    elif argsa.num_shells == "":
        num_shells = 2
    else:
        try:
            num_shells = int(argsa.num_shells)
        except ValueError:
            logger.warning("ERROR in the specified number of SWI echo times: " +
                            num_shells + '. Will use default: 2')
            num_shells = 2


    # Parsing single-shell dMRI B-value argument
    if not argsa.b_value_shell:
        b_value_shell = 1000
    elif argsa.b_value_shell == "":
        b_value_shell = 1000
    else:
        try:
            b_value_shell = int(argsa.b_value_shell)
        except ValueError:
            logger.warning("ERROR in the specified b value for single-shell dMRI: " +
                            b_value_shell + '. Will use default: 1000')
            b_value_shell = 1000

    # Parsing queue argument
    if argsa.queue == []:
        queue = "normal"
    elif argsa.queue == 'normal':
        queue = "normal"
    else:
        queue = argsa.queue.split(",")

    # Check normalisation argument
    norm_check = argsa.norm_check

    # Check complex phase
    complex_phase = argsa.complex_phase

    # Check inverted Phase Encoding Direction for dMRI
    inverted_PED = argsa.inverted_PED

    # Parsing naming pattern argument
    naming_patterns = argsa.naming_patterns
    naming_patterns = naming_patterns.strip()
    if not os.path.exists(naming_patterns):
        logger.error("ERROR: Subject cannot be run. Incorrect naming pattern file specified: " +
                     naming_patterns)
        LT.finishLogging(logger)
        sys.exit(1)

    # Parsing B files argument
    B_files = argsa.B_files
    B_files = B_files.strip()
    if B_files != "":
        if not os.path.exists(B_files):
            logger.error("ERROR: Subject cannot be run. Incorrect B-files directory specified: " +
                         B_files)
            LT.finishLogging(logger)
            sys.exit(1)
        else:
            for fil in ["AP.bval", "AP.bvec" , "PA.bval", "PA.bvec"]:
                if not os.path.exists(B_files + '/' + fil ):
                    logger.error("ERROR: Subject cannot be run. Non-existent B-file: " +
                                 B_files + '/' + fil )
                    LT.finishLogging(logger)
                    sys.exit(1)
        if not os.path.isabs(B_files):
            B_files = os.path.abspath(B_files)

    # Parsing basic QC file argument
    basic_QC_file = argsa.basic_QC_file
    basic_QC_file = basic_QC_file.strip()
    if not os.path.exists(basic_QC_file):
        logger.error("ERROR: Subject cannot be run. Incorrect basic QC file specified: " +
                     basic_QC_file)
        LT.finishLogging(logger)
        sys.exit(1)

    # Parsing execute block argument
    exec_block = argsa.exec_block
    if not argsa.exec_block:
        exec_block = 0
    elif argsa.exec_block == "":
        exec_block = 0
    else:
        try:
            exec_block = int(argsa.exec_block)
            allowed_values = [0, 1, 2, 3, 4, 5, 6, 20, 40, 123]

            if not exec_block in allowed_values:
                logger.warning("ERROR in the eXecute block: " +  exec_block +
                               ': Value not allowed. \nAllowed values: ' +
                               str(allowed_values) + '\nWill use default: 0')
                exec_block = 0

        except ValueError:
            logger.warning("ERROR in the eXecute block: " +
                            exec_block + ': Will use default: 0')
            exec_block = 0



    ################################
    # END OF ARGUMENT PARSING CODE #
    ################################

    # File management and basic QC
    fileConfig = bb_file_manager(logger, subject, True, norm_check,
                                 naming_patterns, coils_SWI, echoes_SWI,
                                 complex_phase, inverted_PED, B_files)
    fileConfig = bb_basic_QC(subject, fileConfig, basic_QC_file)
    logger.info("File configuration after running file manager: " + str(fileConfig))

    # runTopup ==> Having fieldmap
    if not (( ('AP' in fileConfig ) and  (fileConfig['AP'] != '')) and (('PA' in fileConfig ) and  (fileConfig['PA'] != ''))):
        logger.error("There is no proper DWI data. Thus, the B0 file cannot be generated in order to run topup")
        runTopup = False
    else:
        runTopup = True

    # Exit if there is no T1 image
    if not (('T1' in fileConfig ) and  (fileConfig['T1'] != '')):
        logger.error("Subject cannot be run. There is no T1 or it has not the correct dimensions")
        LT.finishLogging(logger)
        exit

    else:
        job = choose_block(subject, runTopup, fileConfig, queue, coeff, num_shells,
                           b_value_shell, exec_block)
        LT.finishLogging(logger)
        print(job)

if __name__ == "__main__":
    main()
