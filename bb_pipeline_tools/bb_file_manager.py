#!/bin/env python

'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 16-Dec-2014 11:51:00
 Version 1.0
 ProjectDir=
 '''

import os
import sys
import glob
import json
import argparse
import bb_pipeline_tools.bb_logging_tool as LT
import bb_file_manager_fnc as fmf

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def run_pattern(logger, pattern_action, list_files, check_normalisation, 
                coils_SWI, echoes_SWI, complex_phase, inverted_PED, B_files):
    pattern = pattern_action['pattern']
    action   = getattr(fmf, pattern_action['action'])
    
    if 'args' in pattern_action:
        args = pattern_action['args']
        if "check_normalisation" in args:
            args.remove('check_normalisation')
            args.append(check_normalisation)
        if "coils_SWI" in args:
            args.remove('coils_SWI')
            args.append(coils_SWI)
        if "echoes_SWI" in args:
            args.remove('echoes_SWI')
            args.append(echoes_SWI)
        if "complex_phase" in args:
            args.remove('complex_phase')
            args.append(complex_phase)
        if "inverted_PED" in args:
            args.remove('inverted_PED')
            args.append(inverted_PED)
        if "B_files" in args:
            args.remove('B_files')
            args.append(B_files)

    else:
        args = []

    list_files=[]
    for pat in pattern:
        list_files.extend([x for x in glob.glob(pat) if x not in list_files])
        logger.info("Performing action " + action.__name__ + 
                    " on files with patterns " + str(pat))
    action(list_files, *args)

def bb_file_manager(logger, subject, check_existence, check_normalisation, 
                    name_patterns_file, coils_SWI, echoes_SWI, complex_phase,
                    inverted_PED, B_files):

    idealConfigFile=os.environ['BB_BIN_DIR'] + '/bb_data/ideal_config.json'
    with open(idealConfigFile, 'r') as f:
        idealConfig=json.load(f)

    fileConfig = {}

    fmf.init_fmf(logger, idealConfig, fileConfig)

    directories=["delete", "unclassified", "raw", "T1", "T2_FLAIR", "SWI", 
                 "SWI/PHA_TE1", "SWI/PHA_TE2", "SWI/MAG_TE1", "SWI/MAG_TE2", 
                 "SWI/unclassified", "dMRI", "dMRI/raw", "fMRI", "fieldmap",  
                 "ASL", "ASL/unclassified"]

    with open(name_patterns_file, 'r') as f:
        patterns = json.load(f)
    
    os.chdir(subject)
    fd_fileName="logs/file_descriptor.json"
    #Check if the subject has already been managed
    if (os.path.isfile(fd_fileName)):
        with open(fd_fileName, 'r') as f:
            fileConfig=json.load(f)

        if check_existence:
        # Check the config file is correct. If not, correct it.
        #keysToDele = [key for key in fileConfig if not os.path.exists(fileConfig[key])]
            keysToDele = [key for key in fileConfig if ((not isinstance(fileConfig[key], list)) and (not os.path.exists(fileConfig[key])))]
            logger.info('Keys to delete: ' + str(keysToDele))
            for key in keysToDele:
                del fileConfig[key]

        fd=open(fd_fileName, "w")
        json.dump(fileConfig,fd,sort_keys=True,indent=4)        
        fd.close()
    
    else:
        for directory in directories:
            if not os.path.isdir(directory):         
                os.mkdir(directory)
        
        list_files=glob.glob("*.*")
        list_files.sort()

        # Process the init pattern
        pattern_action = patterns['init']
        run_pattern(logger, pattern_action, list_files, False, coils_SWI, 
                    echoes_SWI, complex_phase, inverted_PED, B_files)
        
        # Process the main bulk of patterns
        for key in patterns['patterns']:
            pattern_action = patterns['patterns'][key]
            run_pattern(logger, pattern_action, list_files, check_normalisation, 
                        coils_SWI, echoes_SWI, complex_phase, inverted_PED,
                        B_files)

        # Process the ending pattern
        pattern_action = patterns['end']
        run_pattern(logger, pattern_action, list_files, False, coils_SWI, 
                    echoes_SWI, complex_phase, inverted_PED, B_files)

        # Create file descriptor
        fd=open(fd_fileName, "w")
        json.dump(fileConfig,fd,sort_keys=True,indent=4)        
        fd.close()

    os.chdir("..")

    return fileConfig

def main(): 

    parser = MyParser(description='BioBank Pipeline FILE Manager')
    parser.add_argument("subjectFolder", help='Subject Folder')
    parser.add_argument("-n", "--normcheck", action="store_false", default=True,
                        help='Do NOT check Normalisation in structural image (default if flag not used: Check normalisation)', 
                        dest="norm_check")
    parser.add_argument("-P", "--namingPatterns", action="store", nargs="?",
                        default=os.environ['BB_BIN_DIR'] + "/bb_data/naming_pattern_UKBB.json",
                        help='File with the naming patterns coming from dcm2niix (default: UKB naming patterns)', 
                        dest="naming_patterns")
    parser.add_argument("-C", "--coils_SWI", action="store", nargs="?", default=32,
                        help='Number of coils for SWI data. (default: 32) \n' +
                             '  0 means "no separate coil data"', 
                        dest="coils_SWI")
    parser.add_argument("-E", "--echoes_SWI", action="store", nargs="?", default=2,
                        help='Number of echo times for SWI data (default: 2)', 
                        dest="echoes_SWI")
    parser.add_argument("-p", "--complex_phase", action="store_true", default=False,
                        help='SWI phase is complex (default: False)', 
                        dest="complex_phase")
    parser.add_argument("-i", "--inverted_PED", action="store_true", default=False,
                        help='Use if the dMRI Phase Encoding Direction is inverted (default if flag not used: False)', 
                        dest="inverted_PED")
    parser.add_argument("-b", "--b_value_shell", action="store", nargs="?", 
                        default=1000,
                        help='B value for the single shell data to be used (default: 1000)', 
                        dest="b_value_shell")
    parser.add_argument("-B", "--B_files", action="store", nargs="?", 
                        default="",
                        help='Name of the directory with the bval and bvec files for dMRI data.\n' + 
                             'If specified, the directory needs to have these 4 files: \n' +
                             '    - AP.bval\n ' +
                             '    - AP.bvec\n ' +
                             '    - PA.bval\n ' +
                             '    - PA.bvev\n ' +
                             'If not set, the pipeline will use the dcm2niix generated files', 
                        dest="B_files")
    parser.add_argument("-S", "--num_shells", action="store", nargs="?", default=2,
                        help='Number of different shells (B-values) for dMRI data.', 
                        dest="num_shells")

    argsa = parser.parse_args()

    subject = argsa.subjectFolder
    subject = subject.strip()
    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]

    logger = LT.initLogging(__file__, subject)
    logger.info('Running file manager') 

    # Check normalisation argument
    norm_check = argsa.norm_check

    # Check complex phase
    complex_phase = argsa.complex_phase

    # Check inverted Phase Encoding Direction for dMRI
    inverted_PED = argsa.inverted_PED

    
    # Naming pattern argument
    naming_patterns = argsa.naming_patterns
    naming_patterns = naming_patterns.strip()
    if not os.path.exists(naming_patterns):
        logger.error("Subject cannot be run. Incorrect naming pattern file specified: " + 
                      naming_patterns)
        LT.finishLogging(logger)
        exit

    if not argsa.coils_SWI:
        coils_SWI = 32
    elif argsa.coils_SWI == "":
        coils_SWI = 32
    else:
        try:
            coils_SWI = int(argsa.coils_SWI)
        except ValueError:
            logger.warning("Eror in the specified number of SWI coils: " + 
                           coils_SWI + '. Will use default: 32')
            coils_SWI = 32

    if not argsa.echoes_SWI:
        echoes_SWI = 2
    elif argsa.echoes_SWI == "":
        echoes_SWI = 2
    else:
        try:
            echoes_SWI = int(argsa.echoes_SWI)
        except ValueError:
            logger.warning("Eror in the specified number of SWI echo times: " + 
                            echoes_SWI + '. Will use default: 2')
            echoes_SWI = 2


    # Parsing B files argument
    B_files = argsa.B_files
    B_files = B_files.strip()
    if B_files != "":
        if not os.path.exists(B_files):
            logger.error("Subject cannot be run. Incorrect B-files directory specified: " + 
                         B_files)
            LT.finishLogging(logger)
            sys.exit(1)
        else:
            for fil in ["AP.bval", "AP.bvec" , "PA.bval", "PA.bvec"]:
                if not os.path.exists(B_files + '/' + fil ):
                    logger.error("Subject cannot be run. Non-existent B-file: " + 
                                 B_files + '/' + fil )
                    LT.finishLogging(logger)
                    sys.exit(1)
        if not os.path.isabs(B_files):
            B_files = os.path.abspath(B_files)

    fileConfig = bb_file_manager(logger, subject, True, norm_check, 
                                 naming_patterns, coils_SWI, echoes_SWI, 
                                 complex_phase, inverted_PED, B_files)

    logger.info("File configuration after running file manager: \n" + str(fileConfig))

    LT.finishLogging(logger)
             
if __name__ == "__main__":
    main()


