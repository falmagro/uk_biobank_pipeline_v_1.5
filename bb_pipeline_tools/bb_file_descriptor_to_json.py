#!/bin/env python
'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 16-Dec-2014 11:51:00
 Version 1.0
 ProjectDir=
 '''

import os
import json
import bb_logging_tool as LT

class FileConfiguration:
    def __init__(self, T1="", T1_notNorm="", T2="", T2_notNorm="", SWI_TOTAL_PHA="",
                 SWI_TOTAL_MAG="", SWI_TOTAL_MAG_notNorm="", SWI_TOTAL_PHA_TE2="",
                 SWI_TOTAL_MAG_TE2="", SWI_TOTAL_MAG_notNorm_TE2="", SWI_PHA_TE1=None, 
                 SWI_PHA_TE2=None, SWI_MAG_TE1=None, SWI_MAG_TE2=None, rfMRI="", 
                 rfMRI_SBRef="", tfMRI="", tfMRI_SBRef="", AP="", AP_bval="", 
                 AP_bvec="", AP_SBRef="", PA="", PA_bval="", 
                 PA_bvec="", PA_SBRef="", Head_Scout=None):
        
        self={}
        self["T1"]=T1
        self["T1_notNorm"]=T1_notNorm
        self["T2"]=T2
        self["T2_notNorm"]=T2_notNorm
        self["SWI_TOTAL_PHA"]=SWI_TOTAL_PHA
        self["SWI_TOTAL_MAG"]=SWI_TOTAL_MAG
        self["SWI_TOTAL_MAG_notNorm"]=SWI_TOTAL_MAG_notNorm
        self["SWI_TOTAL_PHA_TE2"]=SWI_TOTAL_PHA_TE2
        self["SWI_TOTAL_MAG_TE2"]=SWI_TOTAL_MAG_TE2
        self["SWI_TOTAL_MAG_notNorm_TE2"]=SWI_TOTAL_MAG_notNorm_TE2

        if SWI_PHA_TE1 == None:
            self["SWI_PHA_TE1"]=[]
        else:
            self["SWI_PHA_TE1"]=SWI_PHA_TE1

        if SWI_PHA_TE2 == None:
            self["SWI_PHA_TE2"]=[]
        else:
            self["SWI_PHA_TE2"]=SWI_PHA_TE2

        if SWI_MAG_TE1 == None:
            self["SWI_MAG_TE1"]=[]
        else:
            self["SWI_MAG_TE1"]=SWI_MAG_TE1

        if SWI_MAG_TE2 == None:
            self["SWI_MAG_TE2"]=[]
        else:
            self["SWI_MAG_TE2"]=SWI_MAG_TE2

        self["rfMRI"]=rfMRI
        self["rfMRI_SBRef"]=rfMRI_SBRef
        self["tfMRI"]=tfMRI
        self["tfMRI_SBRef"]=tfMRI_SBRef
        self["AP"]=AP
        self["AP_bval"]=AP_bval
        self["AP_bvec"]=AP_bvec
        self["AP_SBRef"]=AP_SBRef    
        self["PA"]=PA
        self["PA_bval"]=PA_bval
        self["PA_bvec"]=PA_bvec
        self["PA_SBRef"]=PA_SBRef

        if Head_Scout == None:
            self["Head_Scout"]=[]
        else:
            self["Head_Scout"]=Head_Scout


def bb_file_descriptor_to_json(subject):

    logger, logDir =LT.initLogging(__file__, subject) 

    fileConfig=FileConfiguration()

    os.chdir(subject)
    fd_fileName="logs/file_descriptor"

    if not (os.path.isfile(fd_fileName + ".json")):

        if (os.path.isfile(fd_fileName + ".txt")):

            with open(fd_fileName + ".txt") as f:
                content=f.readlines()

            # TODO: Do this MUCH BETTER (Change the printing function to use the labels of the dictionary).
           
            fileConfig["T1"]=content[0].split(':')[1].strip().strip('\n')
            fileConfig["T1_notNorm"]=content[1].split(':')[1].strip().strip('\n')
            fileConfig["T2"]=content[2].split(':')[1].strip().strip('\n')
            fileConfig["T2_notNorm"]=content[3].split(':')[1].strip().strip('\n')
            fileConfig["SWI_TOTAL_PHA_TE1"]=content[4].split(':')[1].strip().strip('\n')
            fileConfig["SWI_TOTAL_MAG_TE1"]=content[5].split(':')[1].strip().strip('\n')
            fileConfig["SWI_TOTAL_MAG_notNorm_TE1"]=content[6].split(':')[1].strip().strip('\n')
            fileConfig["SWI_TOTAL_PHA_TE2"]=content[7].split(':')[1].strip().strip('\n')
            fileConfig["SWI_TOTAL_MAG_TE2"]=content[8].split(':')[1].strip().strip('\n')
            fileConfig["SWI_TOTAL_MAG_notNorm_TE2"]=content[9].split(':')[1].strip().strip('\n')

            if (content[10].split(':')[1].strip().strip('\n').startswith('[')):
                array=content[10].split(':')[1].strip().strip('\n')[1:-1].split(', ')
                fileConfig["SWI_PHA_TE1"]=[s[1:-1] for s in array]
            else:
                fileConfig["SWI_PHA_TE1"]=content[10].split(':')[1].strip().strip('\n')

            if (content[11].split(':')[1].strip().strip('\n').startswith('[')):
                array=content[11].split(':')[1].strip().strip('\n')[1:-1].split(', ')
                fileConfig["SWI_PHA_TE2"]=[s[1:-1] for s in array]
            else:
                fileConfig["SWI_PHA_TE2"]=content[11].split(':')[1].strip().strip('\n')

            if (content[12].split(':')[1].strip().strip('\n').startswith('[')):
                array=content[12].split(':')[1].strip().strip('\n')[1:-1].split(', ')
                fileConfig["SWI_MAG_TE1"]=[s[1:-1] for s in array]            
            else:
                fileConfig["SWI_MAG_TE1"]=content[12].split(':')[1].strip().strip('\n')

            if (content[13].split(':')[1].strip().strip('\n').startswith('[')):
                array=content[13].split(':')[1].strip().strip('\n')[1:-1].split(', ')
                fileConfig["SWI_MAG_TE2"]=[s[1:-1] for s in array]            
            else:
                fileConfig["SWI_MAG_TE2"]=content[13].split(':')[1].strip().strip('\n')


            fileConfig["rfMRI"]=content[14].split(':')[1].strip().strip('\n')
            fileConfig["rfMRI_SBRef"]=content[15].split(':')[1].strip().strip('\n')
            fileConfig["tfMRI"]=content[16].split(':')[1].strip().strip('\n')
            fileConfig["tfMRI_SBRef"]=content[17].split(':')[1].strip().strip('\n')
            fileConfig["AP"]=content[18].split(':')[1].strip().strip('\n')
            fileConfig["AP_bval"]=content[19].split(':')[1].strip().strip('\n')
            fileConfig["AP_bvec"]=content[20].split(':')[1].strip().strip('\n')
            fileConfig["AP_SBRef"]=content[21].split(':')[1].strip().strip('\n')
            fileConfig["PA"]=content[22].split(':')[1].strip().strip('\n')
            fileConfig["PA_bval"]=content[23].split(':')[1].strip().strip('\n')
            fileConfig["PA_bvec"]=content[24].split(':')[1].strip().strip('\n')
            fileConfig["PA_SBRef"]=content[25].split(':')[1].strip().strip('\n')

            if (content[26].split(':')[1].strip().strip('\n').startswith('[')):
                array=content[26].split(':')[1].strip().strip('\n')[1:-1].split(', ')
                fileConfig["Head_Scout"]=[s[1:-1] for s in array]             
            else:
                fileConfig["Head_Scout"]=content[26].split(':')[1].strip().strip('\n')

            fd=open(fd_fileName + '.json', "w")
            json.dump(fileConfig,fd,sort_keys=True,indent=4)        
            fd.close()

            os.chdir("..")

    return fileConfig



