#!/bin/env python

'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 16-Dec-2014 11:51:00
 Version 1.0
 ProjectDir=
 '''

import os
import glob
import json
import numpy as np
import nibabel as nib
import bb_logging_tool as LT
import sys,argparse,os.path
import bb_general_tools.bb_path as bb_path

logger=None
idealConfig={}
fileConfig={}

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def make_unusable(fileName, list_dependent_dirs):

    if fileName.startswith("rfMRI"):
        direc='fMRI'
        os.chdir(direc)
        files_in_dir=glob.glob('./rfMRI*')

        if not 'unusable' in files_in_dir:
            os.mkdir('unusable')
            for file_to_move in files_in_dir:
                os.rename(file_to_move, 'unusable/'+file_to_move)
            f = open('info_rfMRI.txt', 'a')
            f.write('4 0 Missing needed file/modality')
            f.close()

    elif fileName.startswith("tfMRI"):
        direc='fMRI'
        os.chdir(direc)
        files_in_dir=glob.glob('./tfMRI*')

        if not 'unusable' in files_in_dir:
            os.mkdir('unusable')
            for file_to_move in files_in_dir:
                os.rename(file_to_move, 'unusable/'+file_to_move)
            f = open('info_tfMRI.txt', 'a')
            f.write('4 0 Missing needed file/modality')
            f.close()
        

    else:
        for direc in list_dependent_dirs:

            os.chdir(direc)
            files_in_dir=glob.glob('./*')

            if not 'unusable' in files_in_dir:
                os.mkdir('unusable')
                for file_to_move in files_in_dir:
                    os.rename(file_to_move, 'unusable/'+file_to_move)

                f = open('info.txt', 'w')

                if direc=='T1':
                    f.write('2 0 Missing T1')
                else:
                    f.write('4 0 Missing needed modality')
                f.close()
 
            os.chdir('..')
                

def bb_basic_QC(subject, fileConfig, basic_QC_file):

    keysToPop=[]
    global logger

    logger = LT.initLogging(__file__, subject) 

    idealConfigFile = basic_QC_file
    with open(idealConfigFile, 'r') as f:
        idealConfig = json.load(f)

    os.chdir(subject)
    fd_fileName = "logs/file_descriptor.json"

    for fileN in fileConfig:
        if not isinstance(fileConfig[fileN], list):

            if bb_path.isImage(fileConfig[fileN]):
                fils=bb_path.removeImageExt(fileConfig[fileN])   
      
                if os.path.isfile(fils+"_orig.nii.gz"):
                    fileList=[fils+"_orig.nii.gz"]
                else:
                    fileList=[fileConfig[fileN]]

            else:
                fileList=[fileConfig[fileN]]
        else:
            fileList=fileConfig[fileN]

        for fileName in fileList:
            if os.path.isfile(fileName):
                if fileN in idealConfig:
                    img = nib.load(fileName)
                    dims = img.header['dim'][1:5]

                    correctDims=False

                    for idealDims in idealConfig[fileN]['dims']:
                        if np.all(dims == idealDims):
                            correctDims=True
                    if not correctDims:
                        keysToPop.append(fileN)
                        f = open('info_basic_QC.txt', 'a')
                        f.write(subject + ' - Problem in file ' + fileName + ': Dimension found --> ' + str(dims) + '. Dimension expected --> ' + str(idealConfig[fileN]['dims']) + '\n')
                        f.close()

    for keyToPop in keysToPop:
        fileConfig.pop(keyToPop,None)

    fd=open(fd_fileName, "w")
    json.dump(fileConfig,fd,sort_keys=True,indent=4)        
    fd.close()

    os.chdir("..")

    return fileConfig


def main(): 
    parser = MyParser(description='BioBank basic QC tool')
    parser.add_argument("subjectFolder", help='Subject Folder')
    parser.add_argument("-Q", "--basic_QC_file", action="store", nargs="?", 
                        default=os.environ['BB_BIN_DIR'] + "/bb_data/ideal_config_sizes_UKBB.json",
                        help='File with the ideal configuration of the files (default: UKB)', 
                        dest="basic_QC_file")

    argsa = parser.parse_args()
    subject = argsa.subjectFolder
    subject = subject.strip()

    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]
    logger = LT.initLogging(__file__, subject)
    logger.info('Running file manager') 

    # Parsing basic QC file argument
    basic_QC_file = argsa.basic_QC_file
    basic_QC_file = basic_QC_file.strip()

    idealConfigFile = subject+ '/logs/file_descriptor.json'
    if not os.path.exists(idealConfigFile):
        idealConfigFile = os.environ['BB_BIN_DIR'] + "/bb_data/ideal_config.json"

    if not os.path.exists(basic_QC_file):
        logger.error("Subject cannot be run. Incorrect basic QC file specified: " + 
                    basic_QC_file)
        LT.finishLogging(logger)
        sys.exit(1)

    with open(idealConfigFile, 'r') as f:
        fileConfig=json.load(f)

    fileConfig = bb_basic_QC(subject, fileConfig, basic_QC_file)

if __name__ == "__main__":
    main()
 
