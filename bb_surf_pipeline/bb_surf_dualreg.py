#!/usr/bin/env python
'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 15-Dec-2014 18:03:23$
 Version $1.0
 ProjectDir = 
'''

import os
import sys
import argparse
import os.path as op
from pydr import dr
from subprocess import check_output as run
import bb_pipeline_tools.bb_logging_tool as LT

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(): 
  
    parser = MyParser(description='UK BioBank Surface Processing Tool')
    parser.add_argument("subjectFolder", help='Subject Folder')

    argsa = parser.parse_args()

    subject = argsa.subjectFolder
    subject = subject.strip()
    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]

    logger = LT.initLogging(__file__, subject)
    logger.info('Running file manager')

    # Smoothing with an extra 4 FWHM the input
    cmd = [
        f"{os.getenv('BB_BIN_DIR')}/bb_surf_pipeline/bb_smooth_cifti",
        os.environ["PWD"] + "/" + subject + "/surf_fMRI/bb.rfMRI.MSMAll_smooth_2.dtseries.nii",
        "bb.rfMRI.MSMAll_smooth_6.dtseries.nii",
        os.environ["PWD"] + "/" + subject + "/surf_fMRI/",
        "4"
    ]

    run(cmd)

    files = subject + "/surf_fMRI/dualreg_files.csv"
    with open(files, 'w') as f:
        f.write("subid,filename,filename_smooth\n")
        f.write(subject + "," +
            os.environ["PWD"] + "/" + subject + "/surf_fMRI/bb.rfMRI.MSMAll_smooth_2.dtseries.nii" + "," + 
            os.environ["PWD"] + "/" + subject + "/surf_fMRI/bb.rfMRI.MSMAll_smooth_6.dtseries.nii\n")

    for dim in [25, 50]:
        dimstr = str(dim)
        grp_maps = os.environ["BB_BIN_DIR"] + "/bb_data/groupICA_surf_d" + dimstr + "/melodic_IC.dscalar.nii"
        outdir = os.getcwd() + "/" + subject + "/surf_fMRI/ukbb_dim" + dimstr + ".dr"
       
        dr.dr(
               files=files,
               grp_maps=grp_maps,
               workdir=outdir,
               submit_to_cluster=False
        )

if __name__ == "__main__":
    main()
