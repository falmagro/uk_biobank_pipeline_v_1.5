#!/bin/env python

'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 15-Dec-2014 18:03:23$
 Version $1.0
 ProjectDir = 
'''

import os,sys,argparse
import bb_pipeline_tools.bb_logging_tool as LT
import bb_pipeline_tools.bb_file_manager as FM

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def bb_pipeline_surf(subject, jobHold, fileConfiguration, queue):

    logger = LT.initLogging(__file__, subject)
    logDir  = logger.logDir
    baseDir = logDir[0:logDir.rfind('/logs/')]

    if not queue == "normal": 
        short_queues = ','.join(['short.qc@@' + x for x in queue])
        long_queues  = ','.join(['long.qc@@'   + x for x in queue])
    else:
        short_queues = 'short.qc@@short.hge'
        long_queues  = 'long.qc@@long.hge'

    if (not 'T1' in fileConfiguration) or (fileConfiguration['T1'] == ''):
        logger.error('There is no T1. FreeSurfer for subject ' + subject + ' cannot be run.')
        LT.finishLogging(logger)
        return -1


    jobSURF_1 = LT.runCommand(logger, '${FSLDIR}/bin/fsl_sub -q short.qc@@short.hge  -N "bb_surf_' + subject + '" -j ' + str(jobHold)  + ' -l ' + logDir + ' $BB_BIN_DIR/bb_surf_pipeline/bb_surf '  + subject )
    
    return jobSURF_1

def main(): 
  
    parser = MyParser(description='UK BioBank Surface Processing Tool')
    parser.add_argument("subjectFolder", help='Subject Folder')
    parser.add_argument("-n", "--normcheck", action="store_false", default=True,
                        help='Do NOT check Normalisation in structural image (default if flag not used: Check normalisation)', 
                        dest="norm_check")
    parser.add_argument("-P", "--namingPatterns", action="store", nargs="?",
                        default=os.environ['BB_BIN_DIR'] + "/bb_data/naming_pattern_UKBB.json",
                        help='Do NOT check Normalisation in structural image (default if flag not used: Check normalisation)', 
                        dest="naming_patterns")
    parser.add_argument("-q", "--queue", help='Queue modifier (default: normal)',
                        action="store", nargs="?", dest="queue", default="normal")


    argsa = parser.parse_args()

    subject = argsa.subjectFolder
    subject = subject.strip()
    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]

    logger = LT.initLogging(__file__, subject)
    logger.info('Running file manager') 

    # Check normalisation argument
    norm_check = argsa.norm_check
    
    # Naming pattern argument
    naming_patterns = argsa.naming_patterns
    naming_patterns = naming_patterns.strip()
    if not os.path.exists(naming_patterns):
        logger.error("Subject cannot be run. Incorrect naming pattern file specified: " + naming_patterns)
        LT.finishLogging(logger)
        exit

    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]
    
    fileConfig = FM.bb_file_manager(subject, False, norm_check, naming_patterns)

    jobSTEP1 = bb_pipeline_surf(subject, '-1', fileConfig, queue)
    
    print(jobSTEP1)
             
if __name__ == "__main__":
    main()
