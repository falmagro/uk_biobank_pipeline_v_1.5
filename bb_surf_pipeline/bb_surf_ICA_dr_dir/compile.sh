#!/bin/bash

rm   -rf bb_surf_ICA_dr
mkdir -p bb_surf_ICA_dr/for_testing

mcc -o bb_surf_ICA_dr -W main:bb_surf_ICA_dr -T link:exe -d ./bb_surf_ICA_dr/for_testing -v ./bb_netmats.m -a ./FSLNets 
